﻿

// This is a check for the CKEditor class. If not defined, the paths must be checked.
if (typeof CKEDITOR != 'undefined') {
    var editor = CKEDITOR.replace('editor1');
    editor.setData('<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>');

    // Just call CKFinder.setupCKEditor and pass the CKEditor instance as the first argument.
    // The second parameter (optional), is the path for the CKFinder installation (default = "/ckfinder/").
    CKFinder.setupCKEditor(editor, '../');

    // It is also possible to pass an object with selected CKFinder properties as a second argument.
    // CKFinder.setupCKEditor( editor, { basePath : '../', skin : 'v1' } ) ;
}