﻿var myCKEConfig = {
    uiColor: '#ddedf4', // ใช้กับ  skin kama
    language: 'th', // th / en and more..... 
    enterMode: 2,// กดปุ่ม Shift กับ Enter พร้อมกัน 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div> 
    shiftEnterMode: 1,// กดปุ่ม Enter -- 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div>	
    forcePasteAsPlainText: true,   // Paste Pain Text
    toolbarCanCollapse: false,   //not allow toolbar collapse
    resize_enabled: false, //remove resize
    //========== Advance 
     height: 300, // กำหนดความสูง
     width: 730, // กำ//หนดความกว้าง * การกำหนดความกว้างต้องให้เหมาะสมกับจำนวนของ Toolbar
    //removePlugins: 'elementspath', //remove show path
   // stylesSet: 'ckeditor_config.css',
    // protectedSource : ( /<\?[\s\S]*?\?>/g ),   // PHP code
    // FormatSource	 : true,
    // FormatOutput : true,

    toolbar: [   // Start  Set Toolbar  
    ['Maximize', 'Source', 'ShowBlocks', 'SelectAll', 'RemoveFormat'], ['Undo', 'Redo', 'Cut', 'Copy', 'Paste'], ['NewPage', 'Templates', 'Preview', 'Save', 'Print'],
    ['Find', 'Replace'], ['SpecialChar', 'Subscript', 'Superscript', 'HorizontalRule'],
    '/',
    ['Image', 'Link', 'Unlink', 'Table', ],
    ['Bold', 'Italic', 'Underline', 'Strike', 'Outdent', 'Indent'], ['TextColor', 'BGColor'],
    ['FontSize', 'Styles', ],
    ['BulletedList', 'NumberedList', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],

     '/',

    // [ 'Font', 'Format' ,'Smiley'],
    // ['PasteText', 'PasteFromWord'],
    // ['SpellChecker', 'Scayt','Blockquote'],       
    // ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] 
    ],// End  Set Toolbar  
}