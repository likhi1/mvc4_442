﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Web;
//============
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Helper.UT;  // referance method in  folder Helper

namespace MVC442.Models.ViewModel {
    public class SlideManageVM : BaseVM {

        
        public SlideManageVM () {
          
        } 

        [Display(Name = "No.")]
        public int SlideId { get; set; }

        [Display(Name = "ชื่อภาพ")]
        [Required(ErrorMessage = "กรุณาระบุ-SlideName")]
        public string SlideName { get; set; } 
         
        [Required(ErrorMessage = "กรุณาระบุ-SlideLink")]
        [DataType(DataType.MultilineText)]
        public string SlideLink { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-SlideDes")]
        [DataType(DataType.MultilineText)]
        public string SlideDes { get; set; }

        [Display(Name = "สถานะ")]
        public Nullable<int> SlideStatus { get; set; }

         int? _slideSort; 
        [Display(Name = "ลำดับ")]
        public Nullable<int> SlideSort {
            get { return _slideSort; }
            set {
                _slideSort = (value == null) ? 9999 : value;

                //if (value == null) // if user not set  value  ( null )
                //    _slideSort = 9999;  // Set default value   
                //else
                //    _slideSort = value; 

            }
        } 

       [Display(Name = "วันสร้าง")]
      //[DataType(DataType.DateTime)] 
      //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)] 
        public  DateTime?   SlideInputDate { get; set; }

       [Display(Name = "วันเริ่มแสดง")]
       [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SlideDateShow ")]
        public DateTime? SlideDateShow { get; set; }
         
       [Display(Name = "วันหยุดแสดง")]
       [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SlideDateHide")]
        public DateTime?  SlideDateHide { get; set; }

        ///////////////////////////  Create Custom Display Value /////////////////////////////////////////////
        ////======================== Set short name for SlideName 
        [Display(Name = "ชื่อภาพ")]
        [Required(ErrorMessage = "กรุณาระบุ-SlideName")]
        public string SlideNameTrimed {
            get {
                string onlySlideName = SlideName.Replace("/Upload/images/Slide/", "");
                return onlySlideName; 
            }

        }
         
        ///////////////////////////  Create Custom Display Value /////////////////////////////////////////////
   
        DateTime? slideInputDateDisplay;
        public DateTime? SlideInputDateDisplay {
            get {
                slideInputDateDisplay = Utillity.SetLocalTimeZone(SlideInputDate);
                return slideInputDateDisplay;
            }
            set {
                slideInputDateDisplay = value; 
            }
        }

        DateTime? slideDateShowDisplay;
        public DateTime? SlideDateShowDisplay {
            get {
                slideDateShowDisplay = Utillity.SetLocalTimeZone(SlideDateShow);
                return slideDateShowDisplay;
            }

            set {
                slideDateShowDisplay = value;
            }
        }


        DateTime? slideDateHideDisplay;
        public DateTime? SlideDateHideDisplay {
            get {
                slideDateHideDisplay = Utillity.SetLocalTimeZone(SlideDateHide);
                return slideDateHideDisplay;
            }

            set {
                slideDateHideDisplay = value;
            }
        }

 
 
      
    }
}