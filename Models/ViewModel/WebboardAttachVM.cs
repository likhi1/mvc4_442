﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Web; 
//=============
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Diagnostics;

namespace MVC442.Models.ViewModel {
    public class WebboardAttachVM : BaseVM { 
            [Key] 
            public int AttachId { get; set; }
            public Nullable<int> QuizId { get; set; }
            public Nullable<int> AnsId { get; set; }

            [Required(ErrorMessage = "กรุณา อัพโหลดภาพ")]
            [Display(Name = "แนบไฟล์")]
            [ValidateFile]  /// Custom validate  data anotation  
            public IEnumerable<HttpPostedFileBase> AttachName { get; set; }  //HttpPostedFileBase  =>  use  for file upload
         

            public Nullable<System.DateTime> AttachDate { get; set; }

            [Required(ErrorMessage = "กรุณาระบุ ลิงค์")]
            [Display(Name = "ลิงค์")]
            public string AttachLink { get; set; }
        
            [Required(ErrorMessage = "กรุณาระบุ คำบรรยาย")]
            [Display(Name = "คำบรรยาย") ]
            public string AttachAlt { get; set; } 

            public virtual WebboardAnswer WebboardAnswer { get; set; }
            public virtual WebboardQuiz WebboardQuiz { get; set; } 
    }
     

    //public class FileUploadDBModel {
    //    public int Id { get; set; }
    //    public string FileName { get; set; }
    //    public byte[] File { get; set; }
    //}
     
    ///===============  Customized data Annotation Validator for uploading file
    public class ValidateFileAttribute : ValidationAttribute {
      

        public override bool IsValid(object value) { 
            ///////  Set Initail Varible 
            bool checkResult = true; // set default value  befor validate 
            int MaxContentLength = 1024 * 1024 * 3; //3 MB
            string[] AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".gif", ".png" };  // This check on ServerSide
             
            
            ////// Loop get value File Upload
            var FileBase = ((System.Web.HttpPostedFileBase[])(value));
            for (int i = 0; i < FileBase.Count(); i++) {
                HttpPostedFileBase file = FileBase[i];   // ((System.Web.HttpPostedFileBase[])(value))[0].FileName; 
                Debug.WriteLine( file.FileName);
                  
                if (file == null) {
                    checkResult = false;
                } else if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.')))) {
                    ErrorMessage = "Please upload Your Photo of type: " + string.Join(", ", AllowedFileExtensions);
                    checkResult = false;
                } else if (file.ContentLength > MaxContentLength) {
                    ErrorMessage = "Your Photo is too large, maximum allowed size is : " + (MaxContentLength / 1024).ToString() + "MB";
                    checkResult = false;
                }  

            } // end for

            return checkResult;  

        } //end method

    }
     
    //================= Test Validate
    ////// Validate On ViewModel
    //[FileSize(10240)] // test
    //[FileTypes("jpg,jpeg,png")] // test 

    //////// Class Customized Validate 
    public class FileSizeAttribute : ValidationAttribute {
        private readonly int _maxSize;

        public FileSizeAttribute(int maxSize) {
            _maxSize = maxSize;
        }

        public override bool IsValid(object value) {
            if (value == null) return true;

            return (value as HttpPostedFileBase).ContentLength <= _maxSize;
        }

        public override string FormatErrorMessage(string name) {
            return string.Format("The file size should not exceed {0}", _maxSize);
        }
    }
     
    public class FileTypesAttribute : ValidationAttribute {
        private readonly List<string> _types;

        public FileTypesAttribute(string types) {
            _types = types.Split(',').ToList();
        }

        public override bool IsValid(object value) {
            if (value == null) return true;

            var fileExt = System.IO.Path.GetExtension((value as HttpPostedFileBase).FileName).Substring(1);
            return _types.Contains(fileExt, StringComparer.OrdinalIgnoreCase);
        }

        public override string FormatErrorMessage(string name) {
            return string.Format("Invalid file type. Only the following types {0} are supported.", String.Join(", ", _types));
        }
    }
     
}// end namespace