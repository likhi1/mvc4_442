﻿using MVC442.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Helper.UT;  // referance method in  folder Helper

namespace MVC442.Models.ViewModel {

    // [Bind(Exclude = "IdCon")] //\\\\\\\  Lists fields to exclude or include when binding parameter or form values to model properties
    public class WebLinkCasinoVM : BaseVM  {

        // [HiddenInput(DisplayValue = false)] 
        // [ScaffoldColumn(false)] //\\\\\\\  Allows hiding fields from editor formss
        [Display(Name = "IdCon")]
        public int IdCon { get; set; } 
        
        [Required(ErrorMessage = "กรุณาระบุ-ชื่อคาสิโน")]
        [Display(Name = "ชื่อคาสิโน")]
       // public Nullable<int> IdCasinoCat { get; set; }
        public  int  IdCasinoCat { get; set; }  

        [Display(Name = "ประเภท")]
        public Nullable<int> ConType { get; set; } // Speacail  for Adjust Type of  Category  

      //   [Required(ErrorMessage = "กรุณาระบุ-ชื่อลิงค์")] 
         [DataType(DataType.MultilineText)]
        [Display(Name = "ชื่อลิงค์ที่แสดง")]
        public string WebName { get; set; }
         
      // [Required(ErrorMessage = "กรุณาระบุ-ลิงค์URL" )]
        [DataType(DataType.MultilineText)]
        [Display(Name = "ลิงค์ไปที่")]
        public string WebLink { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d.MMM.yyyy  HH:mm }")]
        [DataType(DataType.Date)]
        [Display(Name = "วัน-เวลา") ]
        public Nullable<System.DateTime> InputDate { get; set; }
          
        // [RegularExpression("[0-9]{1,}")] 
        [Display(Name = "ลำดับ")]
        public int? Sort {  get;   set;  } 
         
        [DataType(DataType.MultilineText)]
        [Display(Name = "หมายเหตุ")]
        public string Description { get; set; }


        [Display(Name = "No")]
        public int? No { get; set; } 
        


        public string CatNameThShow { get; set; } // Create this  cause we want  send value by ViewModel


  


        //\\\\\\\\\\\\\\\\\\\  Many to One Relation ( For referrance Casino Csategory )  
        public virtual CasinoCat CasinoCat { get; set; }


 
        ///////////////////////////  Create Custom Display Value ///////////////////////////////////////////// 
        [Display(Name = "ชื่อประเภทลิงค์")]
        public string WebNameDisplay {   ///// Set Trim Message  for Display
            get {
                if ((WebName.Length) > 20) {
                    return WebName.Substring(0, 20) + "...";
                } else {
                    return WebName;
                }
            }
        } 

        [Display(Name = "ชื่อลิงค์")]
        public string WebLinkDisplay {   ///// Set Trim Message  for Display
            get {
                if ((WebLink.Length) > 20) {
                    return WebLink.Substring(0, 20) + "...";
                } else {
                    return WebLink;
                }
            }
        }

        [Display(Name = "ลำดับ")]
        public int? SortDisplay {
            get { ////// Get value too show on View
                if (Sort == 999 || Sort == null) // return null    if  Sort == 999 || Sort  == null  
                    return null;   
                else
                    return Sort;  
            }

            set {////// Set valueon Action Edit 
                Sort = value;
            }
         
        }



        DateTime? inputDateDisplay;
        public DateTime? InputDateDisplay {    /////  Set Format DateTime Display
            get {
                inputDateDisplay = Utillity.SetLocalTimeZone(InputDate);
                return inputDateDisplay;
            }
            set {
                inputDateDisplay = value;
            }
        }

    }
}