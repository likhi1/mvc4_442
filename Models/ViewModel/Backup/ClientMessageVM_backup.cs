﻿using System;
using System.Collections.Generic;  
using System.Linq;
using System.Web;
//============
using System.ComponentModel.DataAnnotations;

namespace MVC442.Models.ViewModel
{
    public class ClientMessageVM_backup
    {  
        [Key]
        [Display(Name = "No.")]
        public int IdCon { get; set; }
          
        [Required(ErrorMessage = "กรุณาระบุ-ประเภท")]
        public Nullable<int> IdCat { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-ชื่อ")]
        [Display(Name = "ชื่อ")]
        public string Name { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-อีเมล์")] 
        [Display(Name = "อีเมล")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-โทรศัพท์")]  
        [Display(Name = "โทรศัพท์")] 
        [StringLength(5 , ErrorMessage = "ระบุไม่เกิน 5 ตัว " ) ]   
        public string Tel1 { get; set; }

        [DataType(DataType.MultilineText)] 
        public string Message { get; set; }

        
        [Display(Name = "วันเวลา")]
        [DataType(DataType.DateTime , ErrorMessage = "Please enter a valid date in the format dd/mm/yyyy hh:mm" )] // create  datetime input
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)] 
        public Nullable<System.DateTime> InputDate { get; set; } //// If want  DateTime  Null Able 
        

        [Display(Name = "IP")]
        public string Ip { get; set; } 
     
        //////// Relation between Table  & for get CatName  ///////////
        public virtual ClientMessageCat ClientMessageCat { get; set; }   //

        //================  Data Anotation ============///
        //xhttp://msdn.microsoft.com/en-us/library/system.componentmodel.dataannotations.datatype.aspx
        ///////// Data Anotation - DataType ///////
        //[CreditCard]
        //[Url]
        //[EmailAddress] 
        //[Integer] 
        //[EqualTo("Password")]
        //[Date]
        //[Digits]    
        //[Numeric] 
        //[DataType(DataType.Url)]
        //[DataType(DataType.Currency)]
        //[DataType(DataType.DateTime)]  
        //[DataType(DataType.PhoneNumber)]
        //[DataType(DataType.EmailAddress)] 
        //[DataType(DataType.MultilineText)]
        //  [DisplayFormat(NullDisplayText = "Gender not specified")] // set name show when field = null
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss tt}")]


        ///////// Data Anotation - Validate  /////////
        //[StringLength(5 , ErrorMessage = "ระบุไม่เกิน 5 ตัว " ) ] 
        //[Range(2, 10 ,  ErrorMessage = "เลขที่ใส่ควรเป็น อย่างน้อย 2 และไมเกิน 10"  ) ] 
        //[DisplayFormat(DataFormatString = "{0:d}")]  
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
         // [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss tt}")]
        //[FileExtensions(Extensions = "jpg,jpeg")] 
        //[FileExtensions("png|jpg|jpeg|gif")]
        //[Min(1, ErrorMessage="Unless you are benjamin button you are lying.")]
        // [DisplayName("Full Name")]
       // [ForeignKey("PersonId")]


        ///////////////////////////////////
        //[HiddenInput(DisplayValue = false)] 
        //  [ScaffoldColumn(false)]   //If you don't want to display a column use ScaffoldColumn attribute. This only works when you use @Html.DisplayForModel() helper


    }

}