﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Web;
//============
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



namespace MVC442.Models.ViewModel {
    public class ClientMessageTelVM {
        [Key]
        [Display(Name = "No.")]
        public int IdCon { get; set; }

       // [ForeignKey("CasinoCategory")] //  Set  ForeignKey 
        public Nullable<int> IdCat { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-โทรศัพท์")]
        [Display(Name = "โทรศัพท์")]
        public string Tel1 { get; set; }

        [Display(Name = "วันเวลา")]
        [DataType(DataType.DateTime)] // create  datetime input
       [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> InputDate { get; set; }

        [Display(Name = "IP")]
        public string Ip { get; set; }
    }
}