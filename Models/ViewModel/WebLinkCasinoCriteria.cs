﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList; 

namespace MVC442.Models.ViewModel {
    public class WebLinkCasinoCriteria : BaseVM {

        public int IdCon { get; set; } 
        public string WebName { get; set; } 
        public string Description { get; set; }
        public int IdCasinoCat { get; set; } 
        public int? Page { get; set; }

        public IPagedList<WebLinkCasinoVM> WebLinkCasinoVMList { get; set; }

    }
}