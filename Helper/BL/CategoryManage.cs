﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Models.ViewModel;

namespace MVC442.Helper.BL  {
    public class CategoryManage : Controller {

        private mvc442Entities db = new mvc442Entities();
        
        //============  Get SelectListItem From Table  CasinoCats  =====================// 
        public IEnumerable<SelectListItem> SelectListItemCasinoCats;

        public  IEnumerable<SelectListItem> GetCasinoCats(int  type  = 0 ) {

            var CatDivide = 3; // Initail Varible for calculate  Content Type 
            if (type == 1) {
                SelectListItemCasinoCats = db.CasinoCats.ToList() //  Set ToList here - FIX  LINQ to Entity not support  .ToString()
                                             .OrderBy(x => x.IdCasinoCat)
                                             .Where(x => x.IdCasinoCat <= CatDivide)
                                             .Select(x => new SelectListItem() { Text = x.CatNameTh, Value = x.IdCasinoCat.ToString()  });
            } else if (type == 2) {
                SelectListItemCasinoCats = db.CasinoCats.ToList() //  Set ToList here - FIX  LINQ to Entity not support  .ToString()
                                             .OrderBy(x => x.IdCasinoCat)
                                             .Where(x => x.IdCasinoCat > CatDivide)
                                             .Select(x => new SelectListItem() { Text = x.CatNameTh, Value = x.IdCasinoCat.ToString() });
            } else {
                SelectListItemCasinoCats = db.CasinoCats.ToList() //  Set ToList here - FIX  LINQ to Entity not support  .ToString()
                                               .OrderBy(x => x.IdCasinoCat)
                                               .Select(x => new SelectListItem() { Text = x.CatNameTh, Value = x.IdCasinoCat.ToString() }); 
            } 
            return SelectListItemCasinoCats; 
        }



        public string GetCatName(int IdCat) {
            var catName = db.CasinoCats.OrderBy(x => x.IdCasinoCat) 
                .Where(x => x.IdCasinoCat == IdCat)
                .FirstOrDefault(); 
            return catName.CatNameTh; 
        } 





    }// end class
}