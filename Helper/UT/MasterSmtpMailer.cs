﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//================ 
using System.Net.Mail;
using System.Net;

namespace MVC442.Helper.UT
{
    public class MasterSmtpMailer
    {

        /////////  Static Field (For Setup  SMTP  outside class )
        public static string MailUsername { get; set; }
        public static string MailPassword { get; set; }
        public static string MailHost { get; set; }
        public static int MailPort { get; set; }
        public static bool MailSSL { get; set; }

        /////////  Public  Field ( For Add form requirement ) 
        public string AddressEmail { get; set; }
        public string DisplayName { get; set; }
        public string CcEmail { get; set; }
        public string BccEmail { get; set; }
        public bool IsHtml { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
       

        public void Send()  {

            SmtpClient smtp = new SmtpClient();
            smtp.Host = MailHost;
            smtp.Port = MailPort; 
            smtp.EnableSsl = MailSSL;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(MailUsername, MailPassword);
  
            MailSSL = false;
            using (var message = new MailMessage())  { //  Create Instance  of MailMessage  
                message.From = new MailAddress(AddressEmail, DisplayName, System.Text.Encoding.UTF8); // Set Send Mail from Hostinguk.net 
                message.Subject = Subject;
                message.Body = Body;
                message.IsBodyHtml = IsHtml; 
                 
                // message.To.Add( ); // Not Necessary  
                message.CC.Add(CcEmail); //  Add email to Cc
                message.Bcc.Add(BccEmail);   //  Add email to Bcc


                smtp.Send(message);

            }
        }



    }
}