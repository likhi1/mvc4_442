﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//========== 
using System.Web.Mvc;
using MVC442.Models;


namespace MVC442.Helper.UT {
    public static class StringExtensions {
         

        public static string SubStringTo(this string thatString, int limit) {
            if (!String.IsNullOrEmpty(thatString)) {
                if (thatString.Length > limit) {
                    return thatString.Substring(0, limit) + "...";  
                }
                return thatString;
            }
            return string.Empty;
        } 
        
 
        /// <summary>
        /// Get substring of specified number of characters on the right.
        /// </summary>s
        public static string Right(this string value, int length) {
            return value.Substring(value.Length - length);
        }


    }
}