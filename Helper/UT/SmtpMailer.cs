﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//================ 
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace MVC442.Helper.UT {
    public class SmtpMailer { 

        /////////  Static Field (For Setup  SMTP  outside class )
        public static string MailUsername { get;  set; }
        public static string MailPassword { get; set; }
        public static string MailHost { get; set; }
        public static int MailPort { get; set; }
        public static bool MailSSL { get; set; }

        /////////  Public  Field ( For Add form requirement )
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; } 
        public string CC { get; set; }
        public string Bcc { get; set; }
         
        //==========  Initail email for Forwerd   

        public string ForwardEmail1 = ConfigurationManager.AppSettings["ForwardEmail1"];
        public string ForwardEmail2 = ConfigurationManager.AppSettings["ForwardEmail2"];

        //////// Set User & Pass for send mail by SMTP 
         static SmtpMailer() { 
            //========  Set  SMTP For Send Mail  
            MailHost = ConfigurationManager.AppSettings["MailHost"];
            MailUsername = ConfigurationManager.AppSettings["MailUsername"];
            MailPassword = ConfigurationManager.AppSettings["MailPassword"];
            MailPort = Convert.ToInt32(ConfigurationManager.AppSettings["MailPort"]);
            MailSSL = false;   

            //======== Send Mail SMTP by Gmail
            //MailUsername = "xxxxx@gmail.com";
            //MailPassword = "xxxxxx";
            //MailHost = "smtp.gmail.com";  // smtp.gmail.com
            //MailPort = 587; // Gmail can use ports 25, 465 & 587; but must be 25 for medium trust environment.
            //MailSSL = true;
        }

        public void Send() {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = MailHost;
            smtp.Port = MailPort;
            smtp.EnableSsl = MailSSL;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(MailUsername, MailPassword);


            using ( var message = new MailMessage( ) ) {  // MailUsername, ToEmail 
                message.From = new MailAddress(MailUsername, "442.im", System.Text.Encoding.UTF8); // Set Send Mail from AspNetHosting.co.uk   
                message.Subject = Subject;
                message.Body = Body;
                message.IsBodyHtml = IsHtml;
                 
                // message.To.Add( ); // Not necessary to assign 
                message.CC.Add(CC); //  Set this send to user 
                message.Bcc.Add(ForwardEmail1);   //  Set this send to P'LEX  (Hide From User See) 
                  
                smtp.Send(message); 

            }
        }
   
    
    
    }
}