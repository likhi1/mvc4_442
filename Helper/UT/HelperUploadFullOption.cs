﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Web;
//=============
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace MVC442.Helper.UT {
    public class HelperUploadFullOption {

        // folder for the upload
        public static readonly string ItemUploadFolderPath = "~/Upload/webboard";

        public static bool renameUploadFile(HttpPostedFileBase file, Int32 counter = 0) {
            
            var fileName = Path.GetFileName(file.FileName);
            string extension = Path.GetExtension(fileName); 
            //======== Rename by  Random String 
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var newFileName = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            var finalFileName = newFileName+extension ;

            //========= If name file exists  => Rename again
            if (System.IO.File.Exists
        (HttpContext.Current.Request.MapPath(ItemUploadFolderPath + finalFileName))) {
                //file exists 
                return renameUploadFile(file, ++counter);
            }
            //file doesn't exist, upload item but validate first
            return uploadFile(file, finalFileName);
        }

        public static bool uploadFile(HttpPostedFileBase file, string fileName) {
            var path =  Path.Combine(HttpContext.Current.Request.MapPath(ItemUploadFolderPath), fileName);
            string extension = Path.GetExtension(file.FileName);

            ///// Make sure the file is valid //////////////
            //if (!validateExtension(extension)) {
            //    return false;
            //}

            try {
                file.SaveAs(path);

                Image imgOriginal = Image.FromFile(path);

                //pass in whatever value you want 
                Image imgActual = ScaleBySize(imgOriginal, 600);
                imgOriginal.Dispose();
                imgActual.Save(path);
                imgActual.Dispose();

                return true;
            } catch (Exception ex) {
                return false;
            }
        }

        private static bool validateExtension(string extension) {
            extension = extension.ToLower();
            switch (extension) {
                case ".jpg":
                    return true;
                case ".png":
                    return true;
                case ".gif":
                    return true;
                case ".jpeg":
                    return true;
                default:
                    return false;
            }
        }

        public static Image ScaleBySize(Image imgPhoto, int size) {
            int logoSize = size;

            float sourceWidth = imgPhoto.Width;
            float sourceHeight = imgPhoto.Height;
            float destHeight = 0;
            float destWidth = 0;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            // Resize Image to have the height = logoSize/2 or width = logoSize.
            // Height is greater than width, set Height = logoSize and resize width accordingly
            if (sourceWidth > (2 * sourceHeight)) {
                destWidth = logoSize;
                destHeight = (float)(sourceHeight * logoSize / sourceWidth);
            } else {
                int h = logoSize / 2;
                destHeight = h;
                destWidth = (float)(sourceWidth * h / sourceHeight);
            }
            // Width is greater than height, set Width = logoSize and resize height accordingly

            Bitmap bmPhoto = new Bitmap((int)destWidth, (int)destHeight,
                                        PixelFormat.Format32bppPArgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, (int)destWidth, (int)destHeight),
                new Rectangle(sourceX, sourceY, (int)sourceWidth, (int)sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();

            return bmPhoto;
        }


    }
}