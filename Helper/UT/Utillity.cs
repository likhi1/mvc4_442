﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
//========== 
using System.Web.Mvc;
using MVC442.Models;



namespace MVC442.Helper.UT { 
    public class Utillity {

        public static HttpBrowserCapabilities RequestBrowser { get { return HttpContext.Current.Request.Browser; } }
        public static string RequestQueryString { get { return Convert.ToString(HttpContext.Current.Request.QueryString); } }

        private mvc442Entities db = new mvc442Entities(); 

        ////// All field
        public object webUrl;
        public object WebUrl {
            get {
                //object rs;
                if (HttpContext.Current != null) {
                    webUrl = HttpContext.Current.Request;
                }
                return webUrl;
            }
        } 

        /// Check mobile device
        public static bool IsMobileDevice() {
             bool result = false;

            if (Utillity.RequestBrowser.IsMobileDevice){  
                result = true;
            } else {
                result = false;
            } 
            return result;
        }

        /// Check Browser Firefox Or Tor Browser
        public static  bool  IsBrowserFirefox() {
            bool result = false; 
            string BrowserName = Utillity.RequestBrowser.Browser;
            if (BrowserName == "Firefox") {
                result = true;
            } else {
                result = false;
            }
            return result;  
        }

        /////  Get TimeZone OnActionExecuting
        public static DateTime? SetLocalTimeZone(DateTime? serverDateTime) {
            var minuteOffset = HttpContext.Current.Session["timezoneoffset"]; // -420 (GMT+7)
            var hoursOffset = Convert.ToInt32(HttpContext.Current.Session["timezoneoffset"]) / 60; // caculate minute to hours
            var hoursOffsetParse = System.Math.Abs(hoursOffset); //  positive integer

            /////// Check  DateTime null  be for use AddHours
            var ResultDatetime = (serverDateTime != null) ? serverDateTime.Value.AddHours(hoursOffsetParse) : serverDateTime; 
            return ResultDatetime;
        }

        public static DateTime? SetServerTimeZone(DateTime? localDateTime) {
            var minuteOffset = HttpContext.Current.Session["timezoneoffset"];// -420 (GMT+7)
            var hoursOffset = Convert.ToInt32(HttpContext.Current.Session["timezoneoffset"]) / 60; // cacula6te minute to hours

            /////// Check  DateTime null  be for use AddHours
            var ResultDatetime = localDateTime != null ? localDateTime.Value.AddHours(hoursOffset) : localDateTime; 
            return ResultDatetime;
        }
         
     public static string BrowserData( ) {
      HttpBrowserCapabilities browser = Utillity.RequestBrowser;
    string browserInfo = "Browser Capabilities\n"
        + "Type = "                    + browser.Type + "\n"
        + "Name = "                    + browser.Browser + "\n"
        + "Version = "                 + browser.Version + "\n"
        + "Major Version = "           + browser.MajorVersion + "\n"
        + "Minor Version = "           + browser.MinorVersion + "\n"
        + "Platform = "                + browser.Platform + "\n"
        + "Is Beta = "                 + browser.Beta + "\n"
        + "Is Crawler = "              + browser.Crawler + "\n"
        + "Is AOL = "                  + browser.AOL + "\n"
        + "Is Win16 = "                + browser.Win16 + "\n"
        + "Is Win32 = "                + browser.Win32 + "\n"
        + "Supports Frames = "         + browser.Frames + "\n"
        + "Supports Tables = "         + browser.Tables + "\n"
        + "Supports Cookies = "        + browser.Cookies + "\n"
        + "Supports VBScript = "       + browser.VBScript + "\n"
        + "Supports JavaScript = "     +   browser.EcmaScriptVersion.ToString() + "\n"
        + "Supports Java Applets = "   + browser.JavaApplets + "\n"
        + "Supports ActiveX Controls = " + browser.ActiveXControls   + "\n"
        + "Supports JavaScript Version = " +  browser["JavaScriptVersion"] + "\n"; 

    return browserInfo;
}

        ///// Get Base 
        public static string GetHost( ) {
            string hostName = (HttpContext.Current.Request.Url.Host).ToString();  // localhost  
            string PortName = (HttpContext.Current.Request.Url.Port).ToString(); // localhost  
            string host = hostName +":"+ PortName+"/";
            return host;  
        } 

    }// end class
}