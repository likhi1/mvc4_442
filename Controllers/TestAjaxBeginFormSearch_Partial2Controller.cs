﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;

namespace MVC442.Controllers
{
    public class TestAjaxBeginFormSearch_Partial2Controller : Controller
    {
     

        private mvc442Entities db = new mvc442Entities();
         
        public ActionResult Index()
        {
            return View();
        }


        //[HttpPost]
        public ActionResult GetData( WebLinkCasino VmModel) {
            var model = db.WebLinkCasinoes
                .Where(f =>  (String.IsNullOrEmpty(VmModel.WebName) || f.WebName.Contains(VmModel.WebName)  ) 
                    &&  (VmModel.IdCasinoCat == 0 || f.IdCasinoCat == VmModel.IdCasinoCat )
                );

            return PartialView("_PartialView", model);  // Return Result to  PartialView 
        }


    }
}