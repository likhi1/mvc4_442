﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
///////////////////////////////
using PagedList;   // set page List
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.BL;
using System.Data.SqlClient;  // referance method in  folder Helper
 


namespace MVC442.Controllers {

    [Authorize]  //\\\\\\\\\\\\  Set Check  Authentication
    public class WebLinkCasinoController : BaseController { // Innherite From BaseController

        //// Get QueryString From  Url /////
        int _type;
        protected int  Type {
            get {
                _type = (Request.QueryString["type"] == null) ? 1 : Convert.ToInt32(Request.QueryString["type"]); 
                return _type;
            }
        }


        private mvc442Entities db = new mvc442Entities();


        public ActionResult index(int type = 0 ) {  
            //var model = db.CasinoCats.Where(x => x.IdCasinoCat <= CatDivide)
            //.Select(x => new CasinoCatVM() {
            //IdCasinoCat = x.IdCasinoCat,
            //CatNameTh = x.CatNameTh
            //}).OrderBy(x => x.IdCasinoCat);

            var CatDivide = 3; // Initail Varible for calculate  Content Type 
            if (type != 2) {
           var model = db.CasinoCats.Where(x => x.IdCasinoCat <= CatDivide)
                    .Select(x => new CasinoCatVM() {
                        IdCasinoCat = x.IdCasinoCat,
                        CatNameTh = x.CatNameTh
                    }).OrderBy(x => x.IdCasinoCat);

             return View(model.ToList());

            } else {
                var model = db.CasinoCats.Where(x => x.IdCasinoCat > CatDivide)
              .Select(x => new CasinoCatVM() {
                  IdCasinoCat = x.IdCasinoCat,
                  CatNameTh = x.CatNameTh
              }).OrderBy(x => x.IdCasinoCat);

                return View(model.ToList());
           } 
          
        }


        public ActionResult GameList(int? cat ) {   ///////   int type = 1  Set Default paramiter when user not assign any paramiter
            // db.CasinoWebLinks.Where(x => x.ConType == Type).Select(x => new { x.IdCon, x.InputDate, x.CatLink }).ToList()  ; 
            var model = db.WebLinkCasinoes
               .Where(x => x.IdCasinoCat == cat)   // Problem frm Rouat 
               .Select(x => new WebLinkCasinoVM {
                   IdCon = x.IdCon,
                   WebName = x.WebName,
                   WebLink = x.WebLink,
                   IdCasinoCat = x.IdCasinoCat,
                   ConType = x.ConType,
                   CatNameThShow = x.CasinoCat.CatNameTh, // Get Name Each Category
                   InputDate = x.InputDate,
                   Sort = x.Sort,
                   Description = x.Description 
                   //\\\\\\\\\\\\\\ test - Get CatNameTh From  db.CasinoCats  
                   /// , CatNameTh = db.CasinoCats.Where(y => y.IdCasinoCat == x.IdCasinoCat ) 
               })
               .OrderBy(x => x.Sort)
               .ThenBy(x => x.IdCon).ToList();

            //========== Add Page PagedList 
            //if (Request.HttpMethod != "GET") {
            //    page = 1;
            //}
            //int pageSize = 25;
            //int pageNumber = (page ?? 1);
            //  ViewBag.Count =  model.Count(); //\\  Count Item From Query 

            //return View(model.ToPagedList(pageNumber, pageSize ));

            return View(model );

        }


        [HttpPost]
        public ActionResult GameListEdit( FormCollection fc , int id) {
           //if (ModelState.IsValid) {
                WebLinkCasino model = db.WebLinkCasinoes.Find(id);

                model.WebLink = Request.Form["item.webLink"];
                model.WebName = Request.Form["item.webName"];

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("GameList", new { type = model.ConType, cat = model.IdCasinoCat });
           // }

           
        }

        public ActionResult IndexPaging(int? page) {   ///////   int type = 1  Set Default paramiter when user not assign any paramiter
            // db.CasinoWebLinks.Where(x => x.ConType == Type).Select(x => new { x.IdCon, x.InputDate, x.CatLink }).ToList()  ; 
            var model = db.WebLinkCasinoes
               .Where(x => x.ConType == Type)   // Problem frm Rouat 
               .Select(x => new WebLinkCasinoVM {
                   IdCon = x.IdCon,
                   WebName = x.WebName,
                   WebLink = x.WebLink,
                   IdCasinoCat = x.IdCasinoCat,
                   ConType = x.ConType,
                   CatNameThShow = x.CasinoCat.CatNameTh, // Get Name Each Category
                   InputDate = x.InputDate,
                   Sort = x.Sort  ,
                   Description = x.Description 

                   //\\\\\\\\\\\\\\ test - Get CatNameTh From  db.CasinoCats  
                   /// , CatNameTh = db.CasinoCats.Where(y => y.IdCasinoCat == x.IdCasinoCat ) 
               }).OrderByDescending(x => x.IdCon).ToList();

            //========== Add Page PagedList 
            if (Request.HttpMethod != "GET") {
                page = 1;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(model.ToPagedList(pageNumber, pageSize));
        }


        /// <summary>
        ///  Test Search LINQ  With Paging
        /// </summary>
        /// <param name="ctr"></param>
        /// <returns></returns>
        public ActionResult IndexSearch(WebLinkCasinoCriteria model) {    

            int pageIndex = model.Page ?? 1; 

            //\\\\\\\  w alias  TableName for LINQ  &  f alias FilterName for LINQ
            model.WebLinkCasinoVMList = (from w in db.WebLinkCasinoes
                                .Where(  f => //\\\\\\  Use LINQ Instaed LIKE in SQL
                                (String.IsNullOrEmpty(model.WebName) || f.WebName.Contains(model.WebName))
                                && (String.IsNullOrEmpty(model.Description) || f.Description.Contains(model.Description))   
                                && (model.IdCasinoCatSelected == 0 ||  f.IdCasinoCat ==  model.IdCasinoCatSelected  )   // for check if default value  = 0 
                                )
                                .OrderBy (f => f.Sort ).ThenByDescending(f => f.IdCon) 
                                         select new WebLinkCasinoVM {
                                             IdCon = w.IdCon,
                                             WebName = w.WebName,
                                             Description = w.Description,
                                             Sort = w.Sort,
                                             InputDate = w.InputDate,
                                             IdCasinoCat = w.IdCasinoCat 
                                         }).ToPagedList(pageIndex, 6 ); 
             

            if (Request.IsAjaxRequest()) {
                return PartialView("_IndexSearchPartial", model);
            } else {
                return View(model);
            }  
        }
      
       
        public ActionResult Create(int cat ) {

           // var model = db.WebLinkCasinoes.Where(x => x.IdCasinoCat == cat).Select( x => x ).First();

            //int value = int.Parse(Entity.TblGold
            //                .OrderByDescending(p => p.Gram)
            //                .Select(r => r.Gram) 
            //                .First().ToString());

            WebLinkCasinoVM VM = new WebLinkCasinoVM {
                ConType = Type,
                IdCasinoCat = cat  
                //No =  model.No 

                //\\\\\\\  Set DropDownListFor Member  
              //  SelectListItems = new CategoryManage().GetCasinoCats(Type)  // Get SelectListItems (DropDownListFor Member) from BaseController.cs
            };

            return View(VM);
        }


        [HttpPost]
        public ActionResult Create(WebLinkCasinoVM VM) {

            if (ModelState.IsValid) {
    
                WebLinkCasino model = new WebLinkCasino {
                    ConType = VM.ConType,
                    WebName = VM.WebName,
                    WebLink = Server.HtmlEncode(VM.WebLink),
                    InputDate = DateTime.Now,
                    No = VM.No ,
                    IdCasinoCat = VM.IdCasinoCat  ,
                   // Sort = VM.Sort ?? 999 , // Set Value if int  == null 
                    Description =  VM.Description ,
                    //\\\\\\   Get  Selected Value  by  BaseVM.cs   
                    //IdCasinoCat = VM.IdCasinoCatSelected
                };

                db.WebLinkCasinoes.Add(model);
                db.SaveChanges();
                return RedirectToAction("GameList", new { type = model.ConType  , cat = model.IdCasinoCat });
            }

            //\\\\\\\  Set DropDownListFor Member 
          //  VM.SelectListItems = new CategoryManage().GetCasinoCats(Type);  // Get SelectListItems (DropDownListFor Member) from BaseController.cs 
            return View(VM);
        }


        [HttpPost]
        public ActionResult EditAjax(WebLinkCasinoVM vm, int? id) {
            WebLinkCasino model = db.WebLinkCasinoes.Find(id);

           //model.WebName = vm.WebName;
           //model.WebLink = Server.HtmlEncode(vm.WebLink);
           //model.InputDate = DateTime.Now;
 

               // Update to database
               db.Entry(model).State = EntityState.Modified;
               db.SaveChanges();
               return RedirectToAction("GameList", new { type = model.IdCasinoCat });

               //if (Request.IsAjaxRequest()) {
               //    return PartialView("_GameListPartial");
               //}

        }


        public ActionResult Edit(int id = 0) {
            WebLinkCasino model = db.WebLinkCasinoes.Find(id);

            WebLinkCasinoVM VM = new WebLinkCasinoVM() {
                IdCon = model.IdCon,
                IdCasinoCat = model.IdCasinoCat ,
                ConType = model.ConType,
                WebName = model.WebName,
                WebLink = model.WebLink,
                No = model.No ,
                //Sort = model.Sort ,
                Description = model.Description ,
                //\\\\\\\\\\\  Set DropDown   SelectListItems  from BaseController.cs  &   Set IdCasinoCatSelected  from  BaseVM.cs  \\\\\\
                // SelectListItems = new CategoryManage().GetCasinoCats(Convert.ToInt32(model.ConType)),   
                // IdCasinoCatSelected = model.IdCasinoCat   
            };

            if (model == null) {
                return HttpNotFound();
            }

            return View(VM);
        }


        [HttpPost]
        public ActionResult Edit(WebLinkCasinoVM VM) {

            if (ModelState.IsValid) {
                WebLinkCasino model = db.WebLinkCasinoes.Find(VM.IdCon);
                model.ConType = VM.ConType;
                model.IdCasinoCat = VM.IdCasinoCat;
                model.WebName = VM.WebName;
                model.WebLink = Server.HtmlEncode(VM.WebLink);
                model.InputDate = DateTime.Now; 
              //  model.Sort = VM.Sort ?? 999 ;// Set Value if int  == null 
                model.Description = VM.Description;
                model.No = VM.No;
                //\\\\\\\\  Get Selected Value from IdCasinoCatSelected From BaseViewModel 
             //   model.IdCasinoCat = VM.IdCasinoCatSelected;

                // Update to database
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("GameList", new { type = model.ConType , cat = model.IdCasinoCat });
            }

            //\\\\\\  Set DropDownListFor Member 
            VM.SelectListItems = new CategoryManage().GetCasinoCats(Convert.ToInt32(VM.ConType));  // Get SelectListItems (DropDownListFor Member) from BaseController.cs
            VM.IdCasinoCatSelected = VM.IdCasinoCat;  // Set Selected DropDown   
            return View(VM);
        }



        [HttpPost]
        public ActionResult Delete(int id) {
            WebLinkCasino casinolink = db.WebLinkCasinoes.Find(id);
            db.WebLinkCasinoes.Remove(casinolink);
            db.SaveChanges();
            return null;
        }


        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}