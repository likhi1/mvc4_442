﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
///////////////////////////////
using PagedList;   // set page List
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.BL;
using System.Data.SqlClient;  // referance method in  folder Helper

namespace MVC442.Controllers
{
    public class TestAjaxBeginFormSearchPagedList2Controller : Controller
    {
        private mvc442Entities db = new mvc442Entities();

        //
        // GET: /TestAjaxBeginForm5/

        public ActionResult Index(WebLinkCasinoCriteria model) {
             
            int pageIndex = model.Page ?? 1;
            // model.IdCasinoCatSelected = model.IdCasinoCatSelected ;

            /// w aliasName Table for LINQ
            /// f aliasName Filter for LINQ
            model.WebLinkCasinoVMList = (from w in db.WebLinkCasinoes
                                .Where(f =>
                                   (String.IsNullOrEmpty(model.WebName) || f.WebName.Contains(model.WebName))
                                && (String.IsNullOrEmpty(model.Description) || f.Description.Contains(model.Description))
                        && (model.IdCasinoCatSelected == 0 || f.IdCasinoCat == model.IdCasinoCatSelected)   // for check if default value  = 0
                                             //  && (  f.IdCasinoCat ==  model.IdCasinoCatSelected  )   // for check if default value  = 0
                                )
                                .OrderBy(f => f.Sort).ThenByDescending(f => f.IdCon)
                                         select new WebLinkCasinoVM {
                                             IdCon = w.IdCon,
                                             WebName = w.WebName,
                                             Description = w.Description,
                                             Sort = w.Sort,
                                             InputDate = w.InputDate,
                                             IdCasinoCat = w.IdCasinoCat
                                         }).ToPagedList(pageIndex, 6);


            if (Request.IsAjaxRequest()) {
                return PartialView("_PartialView", model);
            } else {
                return View(model);
            }  

        }

         
    }
}