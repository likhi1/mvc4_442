﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Models.ViewModel;
using System.Data; 


namespace MVC442.Controllers
{
    //http://stackoverflow.com/questions/5410055/using-ajax-beginform-with-asp-net-mvc-3-razor
    public class TestAjaxBeginForm_Basic1Controller : Controller
    {
         
         public ActionResult index()
         {
             return RedirectToActionPermanent("AjaxViewModel");

         }


         public ActionResult AjaxViewModel( )
         {
             return View();
         }

         [HttpPost]
         public ActionResult AjaxViewModel(ClientMessage  model)
         {
             return Content("Hi " + model.Name + ", Thanks for the details, a mail will be sent to " + model.Message + " with all the login details.", "text/html");
         }

    }//
}
