﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//======================
using MVC442.Models;
using MVC442.Models.ViewModel;

namespace MVC442.Controllers
{
    public class TestPatialDropDown2Controller : Controller
    {

        private mvc442Entities db = new mvc442Entities();

        //\\\\\\\\\  For DropDownList \\\\\\\\\\\\\\\\
        public ActionResult index1(int id = 0){ 
            ViewBag.CasinoCats = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh");// Create SelectList by ViewBag   
 
            return View( );
        }

        //\\\\\\\\\  For DropDownListFor  \\\\\\\\\\\\\\\
        public ActionResult index2(int id = 0){ 
           ViewBag.CasinoCats = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh");// Create SelectList by ViewBag  

            var VM = new BaseVM();  // Set Model by Empty BaseVM  
            VM.SelectListItems = GetCasinoCat();  // Get SelectListItems by  Method GetCasinoCats()
            return View(VM);
        }

        public IEnumerable<SelectListItem> GetCasinoCat()
        {
            var SelectListItemCasinoCats = db.CasinoCats.ToList() //  Set ToList here - FIX  LINQ to Entity not support  .ToString()
                .OrderBy(x => x.IdCasinoCat)
                .Select(x => new SelectListItem() { Text = x.CatNameTh, Value = x.IdCasinoCat.ToString() });
            return SelectListItemCasinoCats;
        }



    }
}