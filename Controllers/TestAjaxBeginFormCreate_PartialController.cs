﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Models.ViewModel; 

namespace MVC442.Controllers
{
    public class TestAjaxBeginFormCreate_PartialController : Controller
    {

   
        mvc442Entities db = new mvc442Entities();

        public ActionResult index()
        {
           // ModelState.AddModelError(string.Empty, "Now You Use AJAX Post"); // Set show error from ModelState
            return RedirectToAction("Create");
               
        }

      
        public ActionResult Create( ) {
            ClientMessageVM VM = new ClientMessageVM();

            return View(VM);
        }

         [HttpPost]
        public ActionResult Create(ClientMessageVM VM) {

           if (  !string.IsNullOrEmpty( VM.Name ) ||  !string.IsNullOrEmpty(VM.Message) ) {
                    ClientMessage model = new ClientMessage();
                    model.Name =  VM.Name ;
                    model.Message =   VM.Message;

                    db.ClientMessages.Add(model);
                    db.SaveChanges();
                                                 

                var model2 = db.ClientMessages.Select( x => new ClientMessageVM {
                    Name = x.Name ,
                    Message = x.Message 
                }).ToList() ;

         

                return PartialView("_PartialView", model2 );  // Return Result to  PartialView  
          }
              
            return View(VM); 
        }
 
 

    }
}
