﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
//==========
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using MVC442.Models;
using MVC442.Models.ViewModel;

namespace MVC442.Controllers {
    public class WebboardController : BaseController {
        private mvc442Entities db = new mvc442Entities();

        //
        // GET: /Webboard/

        public ActionResult Index() {
            return View(db.WebboardQuizs.ToList());
        }

        //
        // GET: /Webboard/Details/5

        public ActionResult Details(int id = 0) {
            WebboardQuiz webboardquiz = db.WebboardQuizs.Find(id);
            if (webboardquiz == null) {
                return HttpNotFound();
            }
            return View(webboardquiz);
        }

        [HttpGet]
        public ActionResult Create() {

            var model = new WebboardQuizVM();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(WebboardQuizVM viewModel) {
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();

            if (String.IsNullOrEmpty(recaptchaHelper.Response)) {
                ModelState.AddModelError("", "Captcha answer cannot be empty.");
                return View(viewModel);
            }

            RecaptchaVerificationResult recaptchaResult =  recaptchaHelper.VerifyRecaptchaResponse();

            if (recaptchaResult != RecaptchaVerificationResult.Success) {
                ModelState.AddModelError("", "Incorrect captcha answer.");
            }


            //===== Is valid Recaptcha ( Google Service )-->
            if (ModelState.IsValid) {



                WebboardQuiz webboardQuiz = new WebboardQuiz();
                webboardQuiz.QuizTopic = viewModel.QuizTopic;
                webboardQuiz.QuizName = viewModel.QuizName;
                webboardQuiz.QuizEmail = viewModel.QuizEmail;
                webboardQuiz.QuizMessage = viewModel.QuizMessage;
                webboardQuiz.QuizIp = Request.UserHostAddress;
                webboardQuiz.QuizDate = DateTime.Now;

                db.WebboardQuizs.Add(webboardQuiz);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(viewModel); //  If not success
        }

        //
        // GET: /Webboard/Edit/5

        public ActionResult Edit(int id = 0) {
            WebboardQuiz webboardquiz = db.WebboardQuizs.Find(id);
            if (webboardquiz == null) {
                return HttpNotFound();
            }
            return View(webboardquiz);
        }

        //
        // POST: /Webboard/Edit/5

        [HttpPost]
        public ActionResult Edit(WebboardQuiz webboardquiz) {
            if (ModelState.IsValid) {
                db.Entry(webboardquiz).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(webboardquiz);
        }

        //
        // GET: /Webboard/Delete/5

        public ActionResult Delete(int id = 0) {
            WebboardQuiz webboardquiz = db.WebboardQuizs.Find(id);
            if (webboardquiz == null) {
                return HttpNotFound();
            }
            return View(webboardquiz);
        }

        //
        // POST: /Webboard/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id) {
            WebboardQuiz webboardquiz = db.WebboardQuizs.Find(id);
            db.WebboardQuizs.Remove(webboardquiz);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}