﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
//======================
using MVC442.Models.ViewModel;

namespace MVC442.Controllers {
    public class TestViewModelController : Controller {
        private mvc442Entities db = new mvc442Entities();

        public ActionResult Index() {
            var model = db.WebLinkCasinoes.ToList().OrderByDescending(x => x.IdCon);
            return View(model);
        }

        public ActionResult Create() {
            ViewBag.IdCasinoCat = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh");
            var VM = new WebLinkCasinoVM();
            return View(VM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WebLinkCasinoVM VM) {
            if (ModelState.IsValid) {
                WebLinkCasino model = new WebLinkCasino {
                    IdCon = VM.IdCon,
                    IdCasinoCat = VM.IdCasinoCat,
                    WebName = VM.WebName,
                    WebLink = VM.WebLink
                };

                db.WebLinkCasinoes.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCasinoCat = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh", VM.IdCasinoCat);
            return View(VM);
        }


        public ActionResult Edit(int id = 0) {
            WebLinkCasino model = db.WebLinkCasinoes.Find(id);
            if (model == null) {
                return HttpNotFound();
            }

            WebLinkCasinoVM VM = new WebLinkCasinoVM {
                IdCon = model.IdCon,
                IdCasinoCat = model.IdCasinoCat,
                WebName = model.WebName,
                WebLink = model.WebLink
            };

            ViewBag.IdCasinoCat = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh", VM.IdCasinoCat);
            return View(VM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(WebLinkCasinoVM VM) {
            if (ModelState.IsValid) {
                var model = db.WebLinkCasinoes.Where(x => x.IdCon == VM.IdCon).SingleOrDefault();
                model.IdCon = VM.IdCon;
                model.IdCasinoCat = VM.IdCasinoCat;
                model.WebName = VM.WebName;
                model.WebLink = VM.WebLink;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCasinoCat = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh", VM.IdCasinoCat);
            return View(VM);
        }

    
        public ActionResult Delete(int id = 0) {
            WebLinkCasino weblinkcasino = db.WebLinkCasinoes.Find(id);
            if (weblinkcasino == null) {
                return HttpNotFound();
            }
            return View(weblinkcasino);
        }

        //
        // POST: /TestViewModel/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            WebLinkCasino weblinkcasino = db.WebLinkCasinoes.Find(id);
            db.WebLinkCasinoes.Remove(weblinkcasino);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}