﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Web;
using System.Web.Mvc;
//=================
using System.Configuration;
using MVC442.Helper;
using System.Diagnostics; 
using System.Net.Mail;
using MVC442.Models ;
using MVC442.Models.ViewModel;
using MVC442.Helper.UT;  // referance method in  folder Helper


namespace MVC442.Controllers
{
    public class TestSendMailerController : Controller
    {
        private mvc442Entities db = new mvc442Entities();  

        public ActionResult Index()  { 
         return View();  
        }
         

        [HttpPost]
        public ActionResult Sendmail(  ClientMessageVM  viewModel ) {
            var responseSendMail = false;  
            if (ModelState.IsValid) {
                //================== Set Model from ViewModel
                ClientMessage clientMessage  = new ClientMessage();
                clientMessage.Name = viewModel.Name;
                clientMessage.Email = viewModel.Email;   // Client Email set Show on  Body
                clientMessage.Tel1 = viewModel.Tel1;
                clientMessage.Message = viewModel.Message;  

                //=============== Process Send mail ==============================//
                try {
                    //\\\\\\\\\\\\ Set  SMTP Variable   
                    MasterSmtpMailer.MailHost = "smtp.gmail.com";  // smtp.gmail.com
                    MasterSmtpMailer.MailPort = 587; // Gmail can use ports 25, 465 & 587; but must be 25 for medium trust environment.
                    MasterSmtpMailer.MailSSL = true;
                    MasterSmtpMailer.MailUsername = "likhi1@gmail.com";  // Set SMTP User & Pass ( Set Static Field  OutSide Class  )  
                    MasterSmtpMailer.MailPassword = "08Nov1978";   // Set SMTP User & Pass ( Set Static Field  OutSide Class  ) 
                    
                    MasterSmtpMailer smtpMailer = new MasterSmtpMailer(); // Create Instance  of Class MaterSmtpMailer  
                    //\\\\\\\\\\\\ Set property of  Instance
                    smtpMailer.AddressEmail = ""; // Address show on title of email
                    smtpMailer.DisplayName = "" ; // Name show on title of email
                    smtpMailer.CcEmail = clientMessage.Email;   // Send back to user
                    smtpMailer.BccEmail = "";    // Email 
                    smtpMailer.Subject = clientMessage.Name;//.Substring(0, 20);
                    smtpMailer.IsHtml = true;
                    smtpMailer.Body = clientMessage.Message;

                    //\\\\\\\\\\\\ Send Mail
                    smtpMailer.Send(); 
                    responseSendMail = true;
                     

                } catch (Exception ex) { 
                    //Debug.WriteLine(ex.Message); 
                    responseSendMail = false; 
                } 
               
            } // end  if (ModelState.IsValid)
            
            if(responseSendMail== true) 
                    ViewData["DisplayMessage"] = "Thank you. We have recieved your query and will get back to you shortly.";
    
            return View();

        } // end method Sendmail


    }
}
