﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

///================
using MVC442.Models;
using MVC442.Models.ViewModel;
using System.Data;

namespace MVC442.Controllers
{
    public class TestPatialDropDown4Controller :  Controller   
    { 

        private mvc442Entities db = new mvc442Entities(); 
 
        public ActionResult index() { 

            return View();
        }
         

        //============  Get SelectListItem From Table  CasinoCats  =====================// 
        public IEnumerable<SelectListItem> SelectListItemCasinoCats;

        public IEnumerable<SelectListItem> GetCasinoCats(int Contype = 0)
        {

            var CatDivide = 3; // Initail Varible for calculate  Content Type 
            if (Contype == 1)
            {
                SelectListItemCasinoCats = db.CasinoCats.ToList() //  Set ToList here - FIX  LINQ to Entity not support  .ToString()
                                             .OrderBy(x => x.IdCasinoCat)
                                             .Where(x => x.IdCasinoCat <= CatDivide)
                                             .Select(x => new SelectListItem() { Text = x.CatNameTh, Value = x.IdCasinoCat.ToString() });
            }
            else if (Contype == 2)
            {
                SelectListItemCasinoCats = db.CasinoCats.ToList() //  Set ToList here - FIX  LINQ to Entity not support  .ToString()
                                             .OrderBy(x => x.IdCasinoCat)
                                             .Where(x => x.IdCasinoCat > CatDivide)
                                             .Select(x => new SelectListItem() { Text = x.CatNameTh, Value = x.IdCasinoCat.ToString() });
            }
            else
            {
                SelectListItemCasinoCats = db.CasinoCats.ToList() //  Set ToList here - FIX  LINQ to Entity not support  .ToString()
                                               .OrderBy(x => x.IdCasinoCat)
                                               .Select(x => new SelectListItem() { Text = x.CatNameTh, Value = x.IdCasinoCat.ToString() });
            }

            return SelectListItemCasinoCats ;

        }

        //============= Set DropdownListFor =========================// 
        public ActionResult CasinoCatsPartial(string DdlIdName, int IdCasinoCat = 0, int type = 0)
        { // For Get Paramiter from  @Html.Action( ) 
            ViewBag.Id = DdlIdName;// Set New IdName  by ViewBag  be Global Var  
            var model = new BaseVM();
            model.IdCasinoCatSelected = IdCasinoCat; // Set  Selected DropDown  in  BaseVM   
            model.SelectListItems = GetCasinoCats(type);

            return PartialView("_CasinoCatsPartial", model); //  Return PartialView   _CasinoCatsPartial  in  Shared Folder
        }
         


    }
}
