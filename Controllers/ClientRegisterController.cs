﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
//===============
using System.Web.Mvc;
using PagedList;   // set page List 
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.BL;  // referance method in  folder Helper

namespace MVC442.Controllers {
    public class ClientRegisterController : BaseController {  // Innherite From BaseController
        private mvc442Entities db = new mvc442Entities();

        //[Authorize(Roles = "Admin")]
        public ActionResult Index( int? page ) {
            #region
            /* 
             //\\\\\\\\\\\\\\  Ex - Get value to  list   By  Linq2SQL
             var model = (from c in db.ClientRegisters 
                            orderby c.IdClientRegister descending  
                            select c).ToList()  ; 

            //\\\\\\\\\\\  Ex - Get value to  list   By   Linq2SQL  & Join Table  ( Get  Name Cetagory  by ForionKey from table CasinoCat )
             var model = from c in db.ClientRegisters
                     join d in db.CasinoCats on c.IdCasinoCat equals d.IdCasinoCat
                     orderby c.IdClientRegister descending    
                     select new ClientRegisterVM { 
                         IdClientRegister = c.IdClientRegister, 
                         Name = c.Name,
                         Email = c.Email,
                         Tel1 = c.Tel1,
                         Ip = c.Ip,
                         InputDate = c.InputDate
                     };  

            //\\\\\\\\\\\\\\ Ex - Get value to  list   By  Linq2Entity
             var model = (db.ClientRegisters.Include(x => x.CasinoCat).OrderByDescending(c => c.IdClientRegister)).ToList();  
             var model = db.ClientRegisters.OrderByDescending(x => x.IdClientRegister).ToList();
            */
            #endregion

            //\\\\\\\\\\\\\\\ Set value to Model Object ( Adventage to use  name colume @Html.DisplayNameFor()  )
             var model =  db.ClientRegisters.OrderByDescending(x=> x.IdClientRegister).Select( x => new ClientRegisterVM { 
                        IdClientRegister = x.IdClientRegister,
                        CatNameThShow = x.CasinoCat.CatNameTh,  // Get Name Each Category
                        Name = x.Name,
                        Email = x.Email,
                        Tel1 = x.Tel1,
                        Ip = x.Ip,
                        InputDate = x.InputDate 
                   });
              
             //========== Add Page PagedList 
           if (Request.HttpMethod != "GET") {
               page = 1;
           }
           int pageSize = 10;
           int pageNumber = (page ?? 1 );

           return View( model.ToPagedList( pageNumber , pageSize ));  
        }   


        public ActionResult Create(int id = 0) {
           
            var model = new ClientRegisterVM{  // Set Model by ViewModel
                //\\\\\\\  Get DropDownListFor Member (SelectListItems)  by type  from BL 
                SelectListItems =  new CategoryManage().GetCasinoCats( )  
            }; 
            return View(model);
        } 

        [HttpPost]
        public ActionResult Create(ClientRegisterVM VM) {
            if (ModelState.IsValid) { 
                var model = new ClientRegister{   
                 Name = VM.Name , 
                 Email = VM.Email ,
                 Tel1 = VM.Tel1 ,
                 GetSms = VM.GetSms ,
                 GetNews = VM.GetNews , 
                 InputDate = DateTime.Now ,
                 Ip = Request.UserHostAddress ,
                 //\\\\\\   Get  Selected Value  by  BaseVM.cs   
                 IdCasinoCat = VM.IdCasinoCatSelected,  
            };
                db.ClientRegisters.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index", "ClientRegister");
            }

            //\\\\\\\  Get DropDownListFor Member (SelectListItems)  by type  from BL 
            VM.SelectListItems = new CategoryManage().GetCasinoCats( );   

            return View(VM);
        }

       
        public ActionResult Edit(int id = 0) {  
            var model = (from c in db.ClientRegisters where c.IdClientRegister == id select c).SingleOrDefault();
  
            ClientRegisterVM VM = new ClientRegisterVM( ) { // Set ViewModel   
                IdClientRegister = model.IdClientRegister,
                Email = model.Email,
                Name = model.Name,
                Tel1 = model.Tel1,
                Ip = model.Ip,
                InputDate = model.InputDate,
                GetNews = model.GetNews,
                GetSms = model.GetSms,
                //\\\\\\\  Get DropDownListFor Member (SelectListItems)  by type  from BL 
                SelectListItems = new CategoryManage().GetCasinoCats( ),   
                IdCasinoCatSelected = model.IdCasinoCat    // Set  Selected DropDown   by  field Inherit from  BaseVM.cs  
            };
             
            return View(VM);
        }
         

        [HttpPost]
        public ActionResult Edit( [Bind(Exclude = "InputDate,Ip")]  ClientRegisterVM VM ) {
            if (ModelState.IsValid) {
                ClientRegister model = db.ClientRegisters.Where(x => x.IdClientRegister == VM.IdClientRegister).SingleOrDefault(); 
                model.Name = VM.Name;
                model.Email = VM.Email;
                model.Tel1 = VM.Tel1;
                model.GetSms = VM.GetSms;
                model.GetNews = VM.GetNews;
                //\\\\\\  Get Selected Value from IdCasinoCatSelected From BaseViewModel
                model.IdCasinoCat = VM.IdCasinoCatSelected;  

                // Update to database
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();   
                return RedirectToAction("Index"); 
            }

            //\\\\\\\  Get DropDownListFor Member (SelectListItems)  by type  from BL 
            VM.SelectListItems = new CategoryManage().GetCasinoCats( );     
            VM.IdCasinoCatSelected = VM.IdCasinoCat; // Set Selected DropDown    
            return View(VM); 
        }


        //
        // GET: /ClientRegister/Details/5 
        public ActionResult Details(int id = 0) {
            ClientRegister model = db.ClientRegisters.Find(id);
            if (model == null) {
                return HttpNotFound();
            }

            ClientRegisterVM VM = new ClientRegisterVM {
                Email = model.Email,
                CatNameThShow = model.CasinoCat.CatNameTh,
                Name = model.Name,
                Tel1 = model.Tel1,
                GetSms = model.GetSms,
                GetNews = model.GetNews,
                InputDate = model.InputDate,
                Ip = model.Ip
            };

            return View(VM);
        }


         
        public ActionResult Delete(int id = 0) {
            ClientRegister clientregister = db.ClientRegisters.Find(id);
            if (clientregister == null) {
                return HttpNotFound();
            }
            return View(clientregister);
        }

        //
        // POST: /ClientRegister/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id) {
            ClientRegister clientregister = db.ClientRegisters.Find(id);
            db.ClientRegisters.Remove(clientregister);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}