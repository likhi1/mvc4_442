﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//==============
using System.Web.Mvc;
using MVC442.Models; 

namespace MVC442.Controllers
{
    public class TestAjaxPartial2Controller : Controller
    {
        private mvc442Entities db = new mvc442Entities();

       
        public ActionResult Index()
        {
            return View();
        }


        public PartialViewResult Germany() {
            var result = from r in db.ClientMessages 
                         select r;
            return PartialView("_DataAjaxCountry", result);
        }

        public PartialViewResult Mexico() {
            var result = from r in db.ClientMessages
                         where r.IdCon <= 2
                         select r;
            return PartialView("_DataAjaxCountry", result);
        }



    }
}
