﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
/////
using MVC442.Models;
using MVC442.Models.ViewModel; 

namespace MVC442.Controllers
{
    public class TestHtmlActionController : Controller
    {

        private mvc442Entities db = new mvc442Entities(); 
         
        public ActionResult Index() {
            return View();
        }
         
        //============================= Use  @Html.Action    Retrive   Data   
        [ChildActionOnly]  // protect retive action direct 
        public ActionResult  LiveScorePartial( int contype ) {  //  
            var model = db.WebLinkLiveScores
            .Where(x => x.ConType == contype)
            .OrderByDescending(x => x.IdCon)
            .Select(x => new WebLinkLiveScoreVM { IdCon = x.IdCon, WebLink = x.WebLink })
            .ToList();

            ////////////////  This Action  Use PartialView For Show Data
            return PartialView("_PartialData", model);
        }

    }
}
