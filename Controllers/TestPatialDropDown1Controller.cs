﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
 //======================
using MVC442.Models;
using MVC442.Models.ViewModel; 

namespace MVC442.Controllers {
    public class TestPatialDropDown1Controller : Controller { 

        public ActionResult Index() { 

            ViewBag.StatusList = DropDownList<Status>.LoadItems(GetData(), "StatusId", "Name");  // Get Dummy Data  from TemplateSelectList
            return View(new CasinoCatVM { IdCasinoCatSelected = 2 });   //   set Selected Value 
        }
        //public ActionResult _PartialView() {
        //    return PartialView();
        //}

        ////// Dummy  
        public List<Status> GetData() {
            var list = new List<Status> { 
                new Status() { StatusId = 1, Name = "AAA" } ,
                new Status() { StatusId = 2, Name = "BBB" } ,
                new Status() { StatusId = 3, Name = "CCC" }  
            };
            return list;
        }
    }
     
    //======== Model Data ( This case Keep  in Controller ) ===========//
    public class Status {
        public decimal StatusId { get; set; }
        public string Name { get; set; }
    }

    ////// For use  selected  
    public class MyViewModel {
        public int StatusId { get; set; }
    }

    ///// For Create SelectList
    public static class DropDownList<T> {
        public static SelectList LoadItems(IList<T> collection, string value, string text) {
            return new SelectList(collection, value, text);
        } 
    }

}
