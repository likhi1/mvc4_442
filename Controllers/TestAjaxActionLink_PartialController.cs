﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
//==============
using System.Web.Mvc;
using MVC442.Models; 


namespace MVC442.Controllers
{
    public class TestAjaxActionLink_PartialController : Controller
    {

       private mvc442Entities db = new mvc442Entities();
 

        public ActionResult Index()
        {
            return View();
        }


        public  PartialViewResult  All() {
            List<ClientMessage> model  =  db.ClientMessages.ToList();
            return PartialView("_DataAjaxPatial" , model );
        }

        public PartialViewResult GetTop2() {
            List<ClientMessage> model = db.ClientMessages.OrderByDescending(x => x.IdCon).Take(2).ToList(); 
            return PartialView("_DataAjaxPatial", model);
        }

        public PartialViewResult GetBottom4() {
            List<ClientMessage> model = db.ClientMessages.OrderBy(x => x.IdCon).Take(4).ToList();
            return PartialView("_DataAjaxPatial", model);
        }

 


    } // end class
}
