﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Models.ViewModel; 
///////////////////////////////
using PagedList;   // set page List
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.BL;
using System.Data.SqlClient;  // referance method in  folder Helper

namespace MVC442.Controllers {
    public class TestSQLCommandController : BaseController {
        private mvc442Entities db = new mvc442Entities();

        //
        // GET: /TestSQLCommand/

        public ActionResult Index() {
            // var weblinkcasinoes = db.WebLinkCasinoes.Include(w => w.CasinoCat);
            string sql = "SELECT *  FROM  dbo.WebLinkCasino";
            var model = db.Database.SqlQuery<WebLinkCasinoVM>(sql).ToList();
            return View(model); 
        }

        //
        // GET: /TestSQLCommand/Details/5

        public ActionResult Details(int id = 0) {
            //  WebLinkCasino weblinkcasino = db.WebLinkCasinoes.Find(id); 
            string sql = "SELECT * FROM  dbo.WebLinkCasino WHERE IdCon = '" + id + "'  ";
            var model = db.Database.SqlQuery<WebLinkCasinoVM>(sql).FirstOrDefault();


            if (model == null) {
                return HttpNotFound();
            }
            return View(model);
        }


        /// <summary>
        ///  Test Search by  Raw SQL  
        /// </summary>
        /// <param name="ctr"></param>
        /// <returns></returns>
        public ActionResult IndexSql(WebLinkCasinoCriteria ctr , int? page) { 
            

            string sql = "SELECT *  FROM  dbo.WebLinkCasino ";

            if (!string.IsNullOrEmpty(ctr.WebName) || !string.IsNullOrEmpty(ctr.Description) || (ctr.IdCasinoCat != 0)) {
                sql += " WHERE   ";
            }
            if (!string.IsNullOrEmpty(ctr.WebName)) {
                sql += " WebName LIKE  '%@WebName%'   AND ";
            } 
            if (!string.IsNullOrEmpty(ctr.Description)) {
                sql += " Description LIKE  '%@Description%'   AND ";
            } 
            if (ctr.IdCasinoCatSelected != 0) {
                sql += " IdCasinoCat =  @IdCasinoCat  AND ";
            } 
            if (sql.EndsWith("AND ") == true){ //  Cut "AND " When SqlParameter have only one paramiter 
                sql = sql.Substring(0, sql.Length - 4); // 
            }
            sql += " ORDER BY sort  ASC ,  IdCon  DESC ";

            //object[] objSqlParameter = {  
            //new SqlParameter("@WebName", ctr.WebName ) ,
            //new SqlParameter("@Description",ctr.Description) ,
            //new SqlParameter("@IdCasinoCat", ctr.IdCasinoCat)  
            //    };

            object[] parameters = new object[3];
            if (ctr.WebName != null) {
                SqlParameter @WebName = new SqlParameter() { ParameterName = "@WebName", DbType = DbType.String, Value = ctr.WebName };
                parameters[0] = @WebName;
            }
            if (ctr.WebName != null) {
                SqlParameter @Description = new SqlParameter() { ParameterName = "@Description", DbType = DbType.String, Value = ctr.Description };
                parameters[1] = @Description;
            }
            if (ctr.WebName != null) {
                SqlParameter @IdCasinoCat = new SqlParameter() { ParameterName = "@IdCasinoCat", DbType = DbType.Int32, Value = ctr.IdCasinoCat };
                parameters[2] = @IdCasinoCat;
            }

            // Run SQL Statement   //  Database.SqlQuery() Not suport Complex ViewModel
            var model = db.Database.SqlQuery<WebLinkCasinoCriteria>(sql, parameters).ToList() ;   


     
                return View(model); 
        }

        //
        // GET: /TestSQLCommand/Create

        public ActionResult Create() {
            ViewBag.IdCasinoCat = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh");
            return View();
        }

        //
        // POST: /TestSQLCommand/Create

        [HttpPost]
        public ActionResult Create(WebLinkCasino weblinkcasino) { 
            if (ModelState.IsValid) {
                db.WebLinkCasinoes.Add(weblinkcasino); 
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCasinoCat = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh", weblinkcasino.IdCasinoCat);
            return View(weblinkcasino);
        }

        //
        // GET: /TestSQLCommand/Edit/5

        public ActionResult Edit(int id = 0) {
            WebLinkCasino weblinkcasino = db.WebLinkCasinoes.Find(id);
            if (weblinkcasino == null) {
                return HttpNotFound();
            }
            ViewBag.IdCasinoCat = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh", weblinkcasino.IdCasinoCat);
            return View(weblinkcasino);
        }

        //
        // POST: /TestSQLCommand/Edit/5

        [HttpPost]
        public ActionResult Edit(WebLinkCasino weblinkcasino) {
            if (ModelState.IsValid) {
                db.Entry(weblinkcasino).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCasinoCat = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh", weblinkcasino.IdCasinoCat);
            return View(weblinkcasino);
        }

        //
        // GET: /TestSQLCommand/Delete/5

        public ActionResult Delete(int id = 0) {
            WebLinkCasino weblinkcasino = db.WebLinkCasinoes.Find(id);
            if (weblinkcasino == null) {
                return HttpNotFound();
            }
            return View(weblinkcasino);
        }

        //
        // POST: /TestSQLCommand/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id) {
            WebLinkCasino weblinkcasino = db.WebLinkCasinoes.Find(id);
            db.WebLinkCasinoes.Remove(weblinkcasino);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}