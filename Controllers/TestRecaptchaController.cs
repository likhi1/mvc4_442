﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
//================
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using System.Threading.Tasks; /// for  ???
using MVC442.Models;
using MVC442.Models.ViewModel;


namespace MVC442.Controllers
{
    public class TestRecaptchaController : Controller {


        //===========================  Basic Recaptcha
        public ActionResult Recaptcha1() {
             var model = new ClientMessageVM();
             return View(model);
        }


        [HttpPost]
        [ActionName("index")]
        public async Task<ActionResult> Recaptcha1(ClientMessageVM model) {
            //=============== Recaptcha Check
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();
            ////// Is Empty
            if (String.IsNullOrEmpty(recaptchaHelper.Response)) {
                ModelState.AddModelError("GoogleRecapcha1", "Captcha answer cannot be empty.");
                return View(model);
            }

            ///////  Is Recaptcha valid
            RecaptchaVerificationResult recaptchaResult = await recaptchaHelper.VerifyRecaptchaResponseTaskAsync();
            if (recaptchaResult != RecaptchaVerificationResult.Success) {
                ModelState.AddModelError("GoogleRecapcha2", "Incorrect captcha answer");
                return View(model); 
            }

 
            return View(model);
        }


        //===========================  Custom  Recaptcha
        public ActionResult Recaptcha2() {
            var model = new ClientMessageVM();
            return View(model);
        }


        [HttpPost]
        [ActionName("index")]
        public async Task<ActionResult> Recaptcha2(ClientMessageVM model) {
            //=============== Recaptcha Check
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();
            ////// Is Empty
            if (String.IsNullOrEmpty(recaptchaHelper.Response)) {
                ModelState.AddModelError("GoogleRecapcha1", "Captcha answer cannot be empty.");
                return View(model);
            }

            ///////  Is Recaptcha  valid
            RecaptchaVerificationResult recaptchaResult = await recaptchaHelper.VerifyRecaptchaResponseTaskAsync();
            if (recaptchaResult != RecaptchaVerificationResult.Success) {
                ModelState.AddModelError("GoogleRecapcha2", "Incorrect captcha answer");
                return View(model);
            }


            return View(model);
        }


        //===========================  Custom  Recaptcha
        public ActionResult Recaptcha3() {
            var model = new ClientMessageVM();
            return View(model);
        }

        [HttpPost]
        [ActionName("index")]
        public async Task<ActionResult> Recaptcha3(ClientMessageVM model) {
            //=============== Recaptcha Check
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();
            ////// Is Empty
            if (String.IsNullOrEmpty(recaptchaHelper.Response)) {
                ModelState.AddModelError("GoogleRecapcha1", "Captcha answer cannot be empty.");
                return View(model);
            }

            ///////  Is Recaptcha  valid
            RecaptchaVerificationResult recaptchaResult = await recaptchaHelper.VerifyRecaptchaResponseTaskAsync();
            if (recaptchaResult != RecaptchaVerificationResult.Success) {
                ModelState.AddModelError("GoogleRecapcha2", "Incorrect captcha answer");
                return View(model);
            }


            return View(model);
        }





    }
}
