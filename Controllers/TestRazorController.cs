﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//=============
using MVC442.Models;

namespace MVC442.Controllers
{
    public class TestRazorController : Controller
    {
        private mvc442Entities db = new mvc442Entities();

        public ActionResult index(int cat = 0) {
            var q = from c in db.WebLinkCasinoes orderby c.IdCon where c.ConType == cat select c; 

            return View(q);
        }
 

    }
}
