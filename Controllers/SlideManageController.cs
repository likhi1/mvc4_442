﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
//===============
using System.Web.Mvc;
using PagedList;
using System.Net;   // set page List 
using System.Globalization;  // DateTime Region
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.UT;  // referance method in  folder Helper

namespace MVC442.Controllers {

    [Authorize]  //\\\\\\\\\\\\  Set Check  Authentication
    public class SlideManageController : BaseController {
        private mvc442Entities db = new mvc442Entities();


        public ActionResult Index(int? page) {
            //============ Set Status - Some Slide  OverDateHide
            CheckOverDate();

            //=========== Get Data to show
            var model = db.SlideManages
                //.Where(x => x.SlideStatus == 1
                //     && (DateTime.Now >= x.SlideDateShow && DateTime.Now <= x.SlideDateHide)
                // )
               // .OrderByDescending(x => x.SlideSort)
                .OrderByDescending(x => x.SlideId)
                .ThenBy(x => x.SlideSort)
              // .OrderBy(x => x.SlideStatus )
              

                .Select(x => new SlideManageVM {
                    SlideId = x.SlideId,
                    SlideName = x.SlideName,
                    SlideSort = x.SlideSort,
                    SlideStatus = x.SlideStatus,
                    SlideDateShow =  x.SlideDateShow  ,
                    SlideDateHide =    x.SlideDateHide  ,
                    SlideInputDate =      x.SlideInputDate   
                });

            //========== Add Page PagedList 
            if (Request.HttpMethod != "GET") {
                page = 1;
            }
            int pageSize = 50;
            int pageNumber = (page ?? 1);

            return View(model.ToPagedList(pageNumber, pageSize));
        }


        public ActionResult TestDatePicker1() {
            return View();
        }
        public ActionResult TestDatePicker2() {
            return View();
        }

        public ActionResult Create() {
            SlideManageVM viewModel = new SlideManageVM();
            return View(viewModel);
        }


        [HttpPost]
        public ActionResult Create(SlideManageVM viewmodel) {
            //viewmodel.SlideName = Request["FilePath"];  
             

            if (ModelState.IsValid) {
                SlideManage slideManages = new SlideManage();

                //DateTime dtDateShow = Convert.ToDateTime(viewmodel.SlideDateShow, new CultureInfo("th-TH"));
                //DateTime dtDateHide = Convert.ToDateTime(viewmodel.SlideDateHide, new CultureInfo("th-TH"));
                //slideManages.SlideDateShow = dtDateShow;
                //slideManages.SlideDateHide = dtDateHide;
                 

                // int intSlideSort = (viewmodel.SlideSort) ?? 9999 ; // Set default value 

                slideManages.SlideName = viewmodel.SlideName;
                slideManages.SlideDes = viewmodel.SlideDes;
                slideManages.SlideSort = viewmodel.SlideSort;
                slideManages.SlideLink = viewmodel.SlideLink;
                slideManages.SlideStatus = viewmodel.SlideStatus; 
                slideManages.SlideInputDate = DateTime.Now;

                 //////// Change To Server DateTime /////
                slideManages.SlideDateShow = Utillity.SetServerTimeZone(viewmodel.SlideDateShow);
                slideManages.SlideDateHide = Utillity.SetServerTimeZone(viewmodel.SlideDateHide);

                db.SlideManages.Add(slideManages);
                db.SaveChanges();
                return RedirectToAction("Index");

            }


            return View(viewmodel);
        }


        public ActionResult Edit(int id = 0) { 

            SlideManage slidemanage = db.SlideManages.Find(id);
            if (slidemanage == null) {
                return HttpNotFound();
            }

            /// int intSlideSort = (slidemanage.SlideSort) ?? 9999; 

            SlideManageVM viewModel = new SlideManageVM {
                SlideId = slidemanage.SlideId,
                SlideName = slidemanage.SlideName,
                SlideLink = slidemanage.SlideLink,
                SlideDes = slidemanage.SlideDes,
                SlideStatus = slidemanage.SlideStatus,
                SlideSort = slidemanage.SlideSort,

                //////// Change To Local DateTime /////
                SlideDateShow = Utillity.SetLocalTimeZone(slidemanage.SlideDateShow ),
                SlideDateHide = Utillity.SetLocalTimeZone(slidemanage.SlideDateHide  )
            };

            return View(viewModel);
        }


        [HttpPost]
        public ActionResult Edit(SlideManageVM slideManageVM) {
            SlideManage slideManage = db.SlideManages.Find(slideManageVM.SlideId);  
             

            if (ModelState.IsValid) {

                slideManage.SlideName = slideManageVM.SlideName;
                slideManage.SlideLink = slideManageVM.SlideLink;
                slideManage.SlideDes = slideManageVM.SlideDes;
                slideManage.SlideStatus = slideManageVM.SlideStatus;
                //int intSlideSort = slideManageVM.SlideSort ?? 9999;   
                //slideManage.SlideSort = intSlideSort;
                slideManage.SlideSort = slideManageVM.SlideSort ?? 9999;  // Set default if user edit value to  NULL 
                slideManage.SlideInputDate = DateTime.Now;

                //////// Change To Server DateTime /////
                slideManage.SlideDateShow = Utillity.SetServerTimeZone(slideManageVM.SlideDateShow);
                slideManage.SlideDateHide = Utillity.SetServerTimeZone(slideManageVM.SlideDateHide);

                db.Entry(slideManage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(slideManageVM);
        }



        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id) {
            SlideManage slidemanage = db.SlideManages.Find(id);
            db.SlideManages.Remove(slidemanage);
            db.SaveChanges();
            return null;
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }



        #region  ///////  Helper ///////// 
        public ActionResult CheckOverDate() {
            //////// Select All Slide 
            var model = db.SlideManages.Select(x => new { x.SlideStatus, x.SlideSort }).ToList();

            foreach (var i in model) {
                //////// Select Slide DateNow  more than  SlideDateHide 
                var c = db.SlideManages.Where(x => x.SlideStatus == 1 && (DateTime.Now >= x.SlideDateHide)).FirstOrDefault();

                if (c != null) { //// Set Status Slide if  DateNow  more than  SlideDateHide 
                    c.SlideStatus = 0;
                    c.SlideSort = 9999;
                    db.SaveChanges();
                } else {//// Set Status Slide if  DateNow  more than  SlideDateHide 
                   // c.SlideStatus = 1;
                   // db.SaveChanges();
                }


            } // end foreach 


            // var model2 = db.SlideManages.ToList(); 
            // return View(model2); 

            return new EmptyResult();
        }

        #endregion


    } // end class
}// end name space