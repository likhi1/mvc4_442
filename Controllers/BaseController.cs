﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.BL;

namespace MVC442.Controllers
{
    public class BaseController : Controller
    {
        private mvc442Entities db = new mvc442Entities();

        //============ Get TimeZone from Cookie to Session =====================//
        protected override void OnActionExecuting(ActionExecutingContext filterContext) {
            if (HttpContext.Request.Cookies.AllKeys.Contains("timezoneoffset")) {
                Session["timezoneoffset"] = HttpContext.Request.Cookies["timezoneoffset"].Value;
            }
            base.OnActionExecuting(filterContext);
        }

         
        //============= Category Manage =========================// 
        //\\\\\\\\\\\\\\\\\\\\\\\ Set DropdownListFor 
        public ActionResult CasinoCatsPartial(string ddlIdName, int IdCasinoCatSelected  = 0 , int type = 0  ) { // For Get Paramiter from  @Html.Action( ) 
            ViewBag.Id = ddlIdName;// Set New IdName  by ViewBag  be Global Var  
            var model = new BaseVM();
            model.IdCasinoCatSelected = IdCasinoCatSelected ; // Set  Selected DropDown  in  BaseVM   
            model.SelectListItems =  new  CategoryManage().GetCasinoCats(type);

            return PartialView("_CasinoCatsPartial", model); //  Return PartialView   _CasinoCatsPartial  in  Shared Folder
        } 

 

    } /// end class
}


