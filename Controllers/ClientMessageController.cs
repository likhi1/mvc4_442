﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
//===============
using System.Web.Mvc; 
using PagedList;
using System.Net;   // set page List
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.UT;  // referance method in  folder Helper

namespace MVC442.Controllers
{
    //[Authorize]
    public class ClientMessageController : BaseController { // Innherite From BaseController

        //private int _type ;
        protected int Type { get { return Convert.ToInt32(Request.QueryString["type"]); } }
      
         
        private mvc442Entities db = new mvc442Entities(); 


        public ActionResult Index( int? page ) {  
             var model = from c in db.ClientMessages
                    where c.IdCat == Type
                    orderby c.IdCon descending
                    select new ClientMessageVM {
                        Email = c.Email ,
                        Name = c.Name ,
                        Tel1 = c.Tel1 ,
                        Message = c.Message ,
                        IdCon = c.IdCon ,
                        Ip = c.Ip ,
                        InputDate = c.InputDate  

                    };

           // var sql = ((System.Data.Objects.ObjectQuery)q).ToTraceString(); // get sql  
   

            //========== Add Page PagedList 
            if (Request.HttpMethod != "GET") {
                page = 1;
            }
            int pageSize = 10 ;
            int pageNumber = (page ?? 1);

            return View(model.ToPagedList(pageNumber, pageSize));  
        }

     

        public ActionResult Details(int id = 0)
        {
            ClientMessage clientmessage = db.ClientMessages.Find(id);
            if (clientmessage == null)
            {
                return HttpNotFound();
            }

            ClientMessageVM viewModel = new ClientMessageVM { 
                IdCon = clientmessage.IdCon ,
                Name = clientmessage.Name,
                Message = clientmessage.Message,
                Tel1 = clientmessage.Tel1,
                Ip = clientmessage.Ip,
                InputDate = Utillity.SetLocalTimeZone( clientmessage.InputDate )

            };

            return View(viewModel);
        }

        //
        // GET: /ClientMessageEmail/Create

        public ActionResult Create()
        {
 
         var models =  new ClientMessageVM()  ;
         models.IdCat = Type;
            return View(models );
        }

      

        [HttpPost]
        public ActionResult Create(ClientMessageVM VM )
        {
            if (ModelState.IsValid)
            { 

                ClientMessage clientMessage = new ClientMessage {
                    Name = VM.Name,
                    Message = VM.Message,
                    Tel1 = VM.Tel1,
                    InputDate = DateTime.Now,
                    Ip = Request.UserHostAddress 
                }; 

                db.ClientMessages.Add(clientMessage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(VM);
        }

        
        public ActionResult Edit(int id = 0)
        {  
           ClientMessage clientmessage = db.ClientMessages.Find(id);

           if (clientmessage == null) { 
               return HttpNotFound(); 
           }
           var models = new ClientMessageVM() {
               IdCon = id ,
               IdCat = Convert.ToInt32(clientmessage.IdCat) ,
               Name = clientmessage.Name,
               Email = clientmessage.Email,
               Tel1 = clientmessage.Tel1,
               Message = clientmessage.Message   
           };
         
            return View(models);

        }
         

        [HttpPost]
        public ActionResult Edit(ClientMessageVM viewModel)  {
            if (ModelState.IsValid) {
                ClientMessage clientMessage = db.ClientMessages.Find(viewModel.IdCon);
                clientMessage.IdCat = viewModel.IdCat;
                clientMessage.Name =  viewModel.Name ;  //  Server.HtmlEncode(viewModel.Name); 
                clientMessage.Email = viewModel.Email;
                clientMessage.Tel1 = Server.HtmlEncode(viewModel.Tel1);
                clientMessage.Message = viewModel.Message; 
                clientMessage.InputDate =  DateTime.Now ;
                  
                db.Entry(clientMessage).State = EntityState.Modified;
                db.SaveChanges();
                RedirectToAction("Index", new { type = Type });
            }
            return View(viewModel);
        }

         

       [HttpPost]
        public ActionResult Delete(int id)  {
            ClientMessage clientmessage = db.ClientMessages.Find(id);
            db.ClientMessages.Remove(clientmessage);
            db.SaveChanges(); 
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}