﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
///================
using MVC442.Models;
using MVC442.Models.ViewModel;
using System.Data;
using MVC442.Helper.BL;

namespace MVC442.Controllers
{
    public class TestPatialDropDown7Controller : BaseController  //\\\\\\  Use Multi Model  by Inherite  BaseController  
    {

        //// Get QueryString From  Url  
        //int _type;
        //protected int Type {
        //    get {
        //        _type = (Request.QueryString["type"] == null) ? 1 : Convert.ToInt32(Request.QueryString["type"]);
        //        return _type;
        //    }
        //} 
         
        private mvc442Entities db = new mvc442Entities();

        public ActionResult Index(int type = 1) {   
            // db.CasinoWebLinks.Where(x => x.ConType == Type).Select(x => new { x.IdCon, x.InputDate, x.CatLink }).ToList()  ; 
            var model = db.WebLinkCasinoes
               .Where(x => x.ConType == type)   // Problem from Route
               .Select(x => new WebLinkCasinoVM {
                   IdCon = x.IdCon,
                   WebName = x.WebName ,
                   IdCasinoCat = x.IdCasinoCat,
                   ConType = x.ConType,
                   CasinoCat = x.CasinoCat  //////  For Get Name Each Category
             
               }).OrderByDescending(x => x.IdCon).ToList();
  
            return View( model);
        }

        public ActionResult Create(int type = 1) { 
            WebLinkCasinoVM VM = new WebLinkCasinoVM {
                ConType = type
            };

            /////// Set DropDownListFor Member 
            VM.SelectListItems = new CategoryManage().GetCasinoCats(type);  // Get SelectListItems (DropDownListFor Member) by type  from BaseController.cs
            return View(VM);
        }


        [HttpPost]
        public ActionResult Create(WebLinkCasinoVM VM ) {  
            if (ModelState.IsValid) {
                WebLinkCasino model = new WebLinkCasino {
                    IdCasinoCat = VM.IdCasinoCatSelected,  //\\\\\\\\\\\\\\  Get  Selected Value  by  BaseVM.cs   
                    ConType = VM.ConType,
                    WebName = VM.WebName,
                    WebLink = Server.HtmlEncode(VM.WebLink),
                    InputDate = DateTime.Now
                };

                db.WebLinkCasinoes.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index", new { type = model.ConType });
            }

            /////// Set DropDownListFor Member 
            VM.SelectListItems = new CategoryManage().GetCasinoCats((int)VM.ConType);   // Get SelectListItems (DropDownListFor Member) by type  from BaseController.cs  
            return View(VM); 
        }
         

        public ActionResult Edit(int id = 0, int type = 1) {
            WebLinkCasino model = db.WebLinkCasinoes.Find(id);

            if (model == null) {
                return HttpNotFound();
            }  
           
            WebLinkCasinoVM VM = new WebLinkCasinoVM() {
                IdCon = model.IdCon,
                ConType = model.ConType,
                WebName = model.WebName,
                WebLink = model.WebLink,
                IdCasinoCat = model.IdCasinoCat,
                IdCasinoCatSelected = model.IdCasinoCat   //\\\\\\\\\\\ Set  Selected Value  to  BaseVM.cs  ( When User Change DropDownList )
            }; 
             
            /////// Set DropDownListFor Member 
            VM.SelectListItems = new CategoryManage().GetCasinoCats((int)(model.ConType)); // Get SelectListItems (DropDownListFor Member) by type  from BaseController.cs  
            return View(VM);
        }
         

        [HttpPost]
        public ActionResult Edit(WebLinkCasinoVM VM) {
            

            if (ModelState.IsValid) {
                WebLinkCasino model = db.WebLinkCasinoes.Find(VM.IdCon);
                model.IdCasinoCat = VM.IdCasinoCatSelected;     //\\\\\\\\\\\\\\  Get  Selected Value  by  BaseVM.cs      
                model.ConType = VM.ConType;
                model.WebName = VM.WebName; 
                model.WebLink = Server.HtmlEncode(VM.WebLink);

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { type = model.ConType });
            }
  
            /////// Set DropDownListFor Member  
            VM.SelectListItems = new CategoryManage().GetCasinoCats((int)(VM.ConType));  // Get SelectListItems (DropDownListFor Member) by type  from BaseController.cs 
            return View(VM);
        }   

    }
}
