﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using System.IO;
//========
using System.Diagnostics; 
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.UT;  // referance method in  folder Helper

namespace MVC442.Controllers {
    public class TestUploadFileController : Controller {

        //=================== Set Field
        string fileName;

        private mvc442Entities db = new mvc442Entities();

        public ActionResult Index() {
            var model = db.WebboardAttaches.ToList().OrderByDescending(x => x.AttachId);
            return View(model);
        }


        //===================================================  uploadSingle
        public ActionResult Edit(int id = 0) {
            var q = (from c in db.WebboardAttaches where c.AttachId == id select c).SingleOrDefault();
            WebboardAttachVM viewModel = new WebboardAttachVM();
            viewModel.AttachAlt = q.AttachAlt;
            viewModel.AttachLink = q.AttachLink;
            //viewModel.AttachName = q.AttachName;
            viewModel.AttachId = q.AttachId;

            return View(viewModel);
        }



        public ActionResult uploadSingle() {
            var model = new WebboardAttachVM();
            return View(model);
        }

        [HttpPost]
        public ActionResult uploadSingle(WebboardAttachVM viewModel) {

            if (ModelState.IsValid) {
                if (Request.Files.Count > 0) {
                    //=========  Get Single Value  by Request.File[index] ========= //
                    var file = Request.Files[0]; //  Refer Request by Index

                    if (file != null && file.ContentLength > 0) {
                        fileName = Path.GetFileName(file.FileName);  ///// Name Original Picture
                        string extension = Path.GetExtension(fileName);
                        var path = Path.Combine(Server.MapPath("~/Upload/webboard"), fileName); ///// Path Upload
                        file.SaveAs(path);
                    }
                }

                WebboardAttach webboardAttach = new WebboardAttach();
                webboardAttach.AttachName = fileName;
                webboardAttach.AttachLink = viewModel.AttachLink;
                webboardAttach.AttachAlt = viewModel.AttachAlt;

                db.WebboardAttaches.Add(webboardAttach);
                db.SaveChanges();

                //========== Option 
                ViewData["Success"] = "File has been uploaded successfully";
                ModelState.Clear();

                return RedirectToAction("Index");
            } // end if (ModelState.IsValid) 


            return View(viewModel);
        }

        //===================================================  uploadMulti
        [HttpGet]
        public ActionResult uploadMulti() {
            var model = new WebboardAttachVM();
            return View(model);
        }

        string finalFileName;
        [HttpPost]
        public ActionResult uploadMulti(WebboardAttachVM viewModel) {
            int countFileUpload = Request.Files.Count;
            if (ModelState.IsValid) {

                //============ Loop For()  for  Multi Upload  ==============//
                for (int i = 0; i < countFileUpload; i++) {

                    //===============  Manage Multi Upload
                    HttpPostedFileBase file = Request.Files[i];
                    //Debug.WriteLine(file.FileName);  
 
                    //=============== Upload & Resize
                    HelperUploadLight  obj = new HelperUploadLight() ;  // create object 
                    obj.SendFileUpload(file); // run method SendFileUpload() in class HelperUploadLight
                    bool result = obj.sucess;  // get varible  sucess  from class HelperUploadLight
                    string fnName = obj.finalFileName;  // get varible  finalFileName  from class HelperUploadLight

                    //=============== Insert upLoadFile Name & Info   to table webboardAttach  
                    if (result  == true ) { 
                        WebboardAttach webboardAttach = new WebboardAttach();
                        webboardAttach.AttachName = fnName ;
                        webboardAttach.AttachLink = viewModel.AttachLink;
                        webboardAttach.AttachAlt = viewModel.AttachAlt;
                        db.WebboardAttaches.Add(webboardAttach);
                        db.SaveChanges(); 
                    } // end  if (result  == true )


                } // end for
                 
                ///////// Set Response Message
                // TempData["Error"] = "This Insert 6Success"; // send data after finish  

                HelperUtility obj2 = new HelperUtility (true); 
                string  x =  obj2.ResponseMessage(true);

                ModelState.Clear();
                return RedirectToAction("Index", new { rs  = true });

            } else {
                ///////// Set Response
                //ViewData["Success"] = "This Insert Not Success"; // send data after finish 
             

                return View(viewModel);

            } // end if (ModelState.IsValid)
             
        }





        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}