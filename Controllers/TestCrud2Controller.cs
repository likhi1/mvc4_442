﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Models.ViewModel;

namespace MVC442.Controllers
{
    public class TestCrud2Controller : Controller
    {
        private mvc442Entities db = new mvc442Entities();

        //
        // GET: /TestCrud/

        public ActionResult Index()
        {
            var clientmessages = db.ClientMessages.Include(c => c.IdCat); 
           
            return View(clientmessages.ToList());
        }

        //
        // GET: /TestCrud/Details/5

        public ActionResult Details(int id = 0)
        {
            ClientMessage clientmessage = db.ClientMessages.Find(id);
            if (clientmessage == null)
            {
                return HttpNotFound();
            }
            return View(clientmessage);
        }

        //
        // GET: /TestCrud/Create

        public ActionResult Create()
        {
            ViewBag.IdCat = new SelectList(db.ClientMessageCats, "IdCat", "DescripTh");
            return View();
        }

        //
        // POST: /TestCrud/Create

        [HttpPost]
        public ActionResult Create(ClientMessage clientmessage)
        {
            if (ModelState.IsValid)
            {
                db.ClientMessages.Add(clientmessage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCat = new SelectList(db.ClientMessageCats, "IdCat", "DescripTh", clientmessage.IdCat);
            return View(clientmessage);
        }

        //
        // GET: /TestCrud/Edit/5

        public ActionResult Edit(int id = 0)
        {


            ClientMessage clientmessage = db.ClientMessages.Find(id);
            if (clientmessage == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCat = new SelectList(db.ClientMessageCats, "IdCat", "DescripTh", clientmessage.IdCat);
            return View(clientmessage);
        }

        //
        // POST: /TestCrud/Edit/5

        [HttpPost]
        public ActionResult Edit(ClientMessage clientmessage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clientmessage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCat = new SelectList(db.ClientMessageCats, "IdCat", "DescripTh", clientmessage.IdCat);
            return View(clientmessage);
        }

        //
        // GET: /TestCrud/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ClientMessage clientmessage = db.ClientMessages.Find(id);
            if (clientmessage == null)
            {
                return HttpNotFound();
            }
            return View(clientmessage);
        }

        //
        // POST: /TestCrud/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ClientMessage clientmessage = db.ClientMessages.Find(id);
            db.ClientMessages.Remove(clientmessage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}