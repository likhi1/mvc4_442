﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//=============
using PagedList;   // set page List
using MVC442.Models;
using MVC442.Models.ViewModel;

namespace MVC442.Controllers
{
    public class BankWebLinkController : BaseController
    {
        private mvc442Entities db = new mvc442Entities();

       
        // GET: /BankLink/
        public ActionResult Index(int? page) { 
            var model = db.BankWebLinks.OrderByDescending(x => x.IdCon).Select(x => new BankWebLinkVM {
                IdCon = x.IdCon,
                BankName = x.BankName,
                BankLink = x.BankLink,
                BankSort = x.BankSort,
                BankStaus  = x.BankStaus ,
                IconPathImage = x.IconPathImage ,
                IconPathFlash = x.IconPathFlash ,
                InputDate  =  x.InputDate 
        }) ; 

            //========== Add Page PagedList 
            if (Request.HttpMethod != "GET") {
                page = 1;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1); 
            return View(model.ToPagedList(pageNumber, pageSize));  
        }
		
     
        //
        // GET: /BankLink/Create 
        public ActionResult Create()  { 
         BankWebLinkVM  VM  =  new BankWebLinkVM(); 
         return View(VM);
        }

        //
        // POST: /BankLink/Create 
        [HttpPost]
        public ActionResult Create(BankWebLinkVM  VM)
        {
            if (ModelState.IsValid)  {
                BankWebLink bankWebLink = new BankWebLink {
               BankLink = VM.BankLink ,
               BankName = VM.BankName ,
              BankSort = VM.BankSort ,
               BankStaus = VM.BankStaus ,
               IconPathImage = VM.IconPathImage,
               IconPathFlash = VM.IconPathFlash,
               InputDate = DateTime.Now   
                };

                db.BankWebLinks.Add(bankWebLink);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(VM);
        }

        //
        // GET: /BankLink/Edit/5 
        public ActionResult Edit(int id = 0){ 
            BankWebLink banklink = db.BankWebLinks.Find(id); 
            BankWebLinkVM VM = new BankWebLinkVM() {
                IdCon = banklink.IdCon, 
                BankName = banklink.BankName,
                BankLink = banklink.BankLink,
                BankSort = banklink.BankSort,
                BankStaus = banklink.BankStaus ,
                IconPathImage = banklink.IconPathImage ,
                IconPathFlash = banklink.IconPathFlash
            };     

            if (banklink == null) {
                return HttpNotFound();
            } 
            return View(VM); 
        }

        //
        // POST: /BankLink/Edit/5 
        [HttpPost]
        public ActionResult Edit(BankWebLinkVM VM)
        {
            if (ModelState.IsValid)
            {
                BankWebLink bankLink = db.BankWebLinks.Find(VM.IdCon);
                bankLink.BankName = VM.BankName;
                bankLink.BankLink = Server.HtmlEncode(VM.BankLink);
                bankLink.BankSort = VM.BankSort;
                bankLink.BankStaus = VM.BankStaus;
                bankLink.IconPathImage = VM.IconPathImage;
                bankLink.IconPathFlash = VM.IconPathFlash;
                bankLink.InputDate = DateTime.Now;

                db.Entry(bankLink).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(VM);
        }
         

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BankWebLink banklink = db.BankWebLinks.Find(id);
            db.BankWebLinks.Remove(banklink);
            db.SaveChanges();
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}