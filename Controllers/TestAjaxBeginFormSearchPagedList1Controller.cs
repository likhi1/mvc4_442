﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using PagedList;
using MVC442.Models.ViewModel;

namespace MVC442.Controllers {
    public class TestAjaxBeginFormSearchPagedList1Controller : Controller {
        private mvc442Entities db = new mvc442Entities();

        //
        // GET: /TestAjaxBeginForm5/

        public ActionResult Index(WebLinkCasinoCriteria model) {
 
            int pageIndex = model.Page ?? 1;
    
            model.WebLinkCasinoVMList = (from w in db.WebLinkCasinoes.Where(f =>
                         (String.IsNullOrEmpty(model.WebName) || f.WebName.Contains(model.WebName))
                         &&  (String.IsNullOrEmpty(model.Description) || f.Description.Contains(model.Description))
                         ).OrderByDescending(f => f.IdCon)
                               select new WebLinkCasinoVM {
                                   IdCon = w.IdCon,
                                   WebName = w.WebName,
                                   Description = w.Description 

                               }).ToPagedList(pageIndex, 5);

 

            if (Request.IsAjaxRequest()) {
                return PartialView("_PartialView", model);
            } else {
                return View(model);
            }  
             
        }

         
    

    }
}