﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//======================
using MVC442.Models;
using MVC442.Models.ViewModel;



namespace MVC442.Controllers
{
    public class TestPatialDropDown3Controller : Controller {
        private mvc442Entities db = new mvc442Entities();

        
 
        public ActionResult index(int id = 0)
        {

            var VM = new BaseVM();  // Get Field SelectListItems  From Empty BaseVM 
            VM.SelectListItems = GetCasinoCat();  // Get SelectListItems by  Method GetCasinoCats()
            return View(VM);
        }
         
        public IEnumerable<SelectListItem> GetCasinoCat()
        {
            var SelectListItemCasinoCats = db.CasinoCats.ToList() //  Set ToList here - FIX  LINQ to Entity not support  .ToString()
                .OrderBy(x => x.IdCasinoCat)
                .Select(x => new SelectListItem() { Text = x.CatNameTh, Value = x.IdCasinoCat.ToString() });
            return SelectListItemCasinoCats;
        }



    }
}
