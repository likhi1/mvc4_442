﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Models.ViewModel;
using System.Data;

namespace MVC442.Controllers
{
    public class TestAjaxBeginFormCreateUpdateDeleteController : Controller
    {
        mvc442Entities db = new mvc442Entities();

        public ActionResult Index()  {
            var model = db.ClientMessages.ToList(); 
            return View(model) ;
        }

        public ActionResult Create(int id=0)  { 

            var VmModel = new ClientMessageVM();
            return View(VmModel); 
        }
        
        
        [HttpPost]
        public ActionResult Create(ClientMessage VM )  {
  
   ClientMessage model = new ClientMessage();
            if (ModelState.IsValid) {
                    model.Name = VM.Name;
                    model.Message = VM.Message;

                db.ClientMessages.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index"); 
              } 

             return View(VM);
        }

        public ActionResult Edit(int id = 0) {
            ClientMessage clientMessage = db.ClientMessages.Find(id);

            var VmModel = new ClientMessageVM {
                IdCon = clientMessage.IdCon,
                Name = clientMessage.Name,
                Message = clientMessage.Message

            };

            return View(VmModel);
        }


        [HttpPost]
        public ActionResult Edit(ClientMessage VM) {
            ClientMessage clientMessages = db.ClientMessages.Find(VM.IdCon);

            clientMessages.Name = VM.Name;
            clientMessages.Message = VM.Message;

            //////  Insert to database
            db.Entry(clientMessages).State = EntityState.Modified;
            db.SaveChanges();
            return View();

        }


    }
}
