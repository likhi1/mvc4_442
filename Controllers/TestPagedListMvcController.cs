﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
using PagedList;

namespace MVC442.Controllers
{
    public class TestPagedListMvcController : Controller
    {
        private mvc442Entities db = new mvc442Entities();

        //
        // GET: /TestPagedListMvc/

        public ActionResult Index(int? page)
        {
            var model = db.WebLinkCasinoes.Include(w => w.CasinoCat).ToList() ; 

            //========== Add Page PagedList  
            if (Request.HttpMethod != "GET") {
                page = 1;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(model.ToPagedList(pageNumber, pageSize)); 


        }

     
    }
}