﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//=============
using PagedList;   // set page List
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.BL;  // referance method in  folder Helper

namespace MVC442.Controllers
{
    [Authorize]  //\\\\\\\\\\\\  Set Check  Authentication
    public class WebLinkLiveScoreController : BaseController { // Innherite From BaseController
       
        protected int Type { get { return Convert.ToInt32(Request.QueryString["type"]); } }

        private mvc442Entities db = new mvc442Entities();

        public IEnumerable<CasinoCat> PageName() {
            IEnumerable<CasinoCat> casinoCat = db.CasinoCats.ToList().AsEnumerable();
            return casinoCat;
        }

        public ActionResult Index(int? page) {
            // db.CasinoWebLinks.Where(x => x.ConType == Type).Select(x => new { x.IdCon, x.InputDate, x.CatLink }).ToList()  ; 
            var model = db.WebLinkLiveScores 
               .Where(x => x.ConType == Type)   // Problem frm Rouat 
               .Select(x => new WebLinkLiveScoreVM {
                   IdCon = x.IdCon,
                   ConType = x.ConType,
                   WebName = x.WebName ,
                   WebLink = x.WebLink, 
                   InputDate = x.InputDate
               }).OrderByDescending(x => x.IdCon).ToList();

            //========== Add Page PagedList 
            if (Request.HttpMethod != "GET") {
                page = 1;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(model.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Details(int id = 0) {
            WebLinkLiveScore webLinkLiveScores = db.WebLinkLiveScores.Find(id);
            if (webLinkLiveScores == null) {
                return HttpNotFound();
            }
            return View(webLinkLiveScores);
        }
         
        public ActionResult Create() {
            WebLinkLiveScoreVM VM = new WebLinkLiveScoreVM {
                ConType = Type
            }; 
            return View(VM);
        }
         
        [HttpPost]
        public ActionResult Create(WebLinkLiveScoreVM VM ) {
            if (ModelState.IsValid) {
                WebLinkLiveScore model = new WebLinkLiveScore {
                    WebName = VM.WebName,
                    ConType = VM.ConType,
                    WebLink = Server.HtmlEncode(VM.WebLink),
                    InputDate = DateTime.Now
                };

                db.WebLinkLiveScores.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index", new { type = model.ConType });
            } 
            return View(VM);
        }
         

        public ActionResult Edit(int id = 0) {
            WebLinkLiveScore model  = db.WebLinkLiveScores.Find(id);

            WebLinkLiveScoreVM VM = new WebLinkLiveScoreVM() {
                IdCon = model.IdCon,
                WebName = model.WebName,
                ConType = model.ConType,
                WebLink = model.WebLink
            };

            if (model == null) {
                return HttpNotFound();
            }
                return View(VM);
        }


        [HttpPost]
        public ActionResult Edit(WebLinkLiveScoreVM VM) {

            if (ModelState.IsValid) {
                WebLinkLiveScore model = db.WebLinkLiveScores.Find(VM.IdCon);
                model.WebName = VM.WebName;
                model.ConType = VM.ConType;
                model.WebLink = Server.HtmlEncode(VM.WebLink);
                model.InputDate = DateTime.Now;


                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { type = model.ConType });
            }


             return View(VM);
        }


        [HttpPost]
        public ActionResult Delete(int id) {
            WebLinkLiveScore model = db.WebLinkLiveScores.Find(id);
            db.WebLinkLiveScores.Remove(model);
            db.SaveChanges();
            return null;
        }


        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }




    } 

}
