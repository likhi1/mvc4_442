
    $(function () {
        /////// Cone  recaptcha
        //$('form').after($('form').clone()  );    

        ////// execute reload Captcha
        function reloadCaptcha(challenge) {
            $(':input[name=recaptcha_response_field]').val(''); // clear text in input befor reload
            $(':input[name=recaptcha_challenge_field]').val(challenge); // Set  challenge
            $('img.recaptcha').attr('src', '//www.google.com/recaptcha/api/image?c=' + challenge); // Set image recaptcha
            console.log(challenge);
        }
        /////  execute   after recaptcha reload
        Recaptcha.finish_reload = function (challenge, b, c) {
            reloadCaptcha(challenge);
        }

        //////  execute callback
        Recaptcha.challenge_callback = function () {
            reloadCaptcha(RecaptchaState.challenge);
        }

        ///// create recaptcha 
        //System.Configuration.ConfigurationManager.AppSettings["recaptchaPublicKey"]
       // Recaptcha.create("6LeGmPQSAAAAAHRTGgXg-4XlxMJtijsKjrNfA_P9");  // recaptcha google plublic key

        ///// event  on get   another recaptcha
        $(document).on('click', '.reload_captcha', function (e) {
            e.preventDefault();
            Recaptcha.reload();
        });

    });//  ready  end 