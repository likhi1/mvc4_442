﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC442 {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //        name: "CustomRoute1",
            //        url: "{controller}/{action}/{type}/{idCat}",
            //        defaults: new { controller = "Home", action = "Index", type = UrlParameter.Optional, idCat = UrlParameter.Optional }
            //    ); 
             
            //====== Case - Detech Action Only When  Controller = Home 
            //routes.MapRoute(
            //name: "OnlyAction",
            //url: "{action}",
            //defaults: new { controller = "home", action = "index", id = UrlParameter.Optional }
            //);

            ////// Master Route /////////////// 
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                //////  Beware  If name "id"  will diplay at the end 
                ///// http://localhost:41208/Home/CasinoOnline/4?type=2
            );

 

        }
    }
}