﻿using System.Web;
using System.Web.Optimization;

namespace MVC442 {
    public class BundleConfig {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));


            ////// Load Validtae & All unobtrusive (Validtae Unobtrusive  , AJAX Unobtrusive )
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                /////////////// Select Manual 
                       "~/Scripts/jquery.unobtrusive-ajax.js",  // Have to use code in jquery.unobtrusive-ajax.js  vision  
                        "~/Scripts/jquery.validate-vsdoc.js",
                        "~/Scripts/jquery.validate.js",
                        "~/Scripts/jquery.validate.unobtrusive.js"  

                        ));
             
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(   //  Use with  =>   @Styles.Render("~/Content/themes/base/css")
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                         "~/Content/themes/base/jquery.ui.dialog.css" 

                        //"~/Content/themes/base/jquery.ui.resizable.css",
                        //"~/Content/themes/base/jquery.ui.selectable.css",
                        //"~/Content/themes/base/jquery.ui.accordion.css",
                        //"~/Content/themes/base/jquery.ui.autocomplete.css",
                        //"~/Content/themes/base/jquery.ui.button.css",
                        //"~/Content/themes/base/jquery.ui.slider.css",
                        //"~/Content/themes/base/jquery.ui.tabs.css",
                        //"~/Content/themes/base/jquery.ui.progressbar.css",
                        //"~/Content/themes/base/jquery.ui.theme.css"
                        ));
              

        //============================== 442.im  CSS =========================//
        ///////  Get All .css  From  Content Folder Not With All SubDirectorys
        bundles.Add(new StyleBundle("~/Content/css").Include(  // Use with  =>  @Styles.Render("~/Content/css")
          
        ));


        /////// Select .css  From  442styles Folder Not With All SubDirectorys  
        bundles.Add(new StyleBundle("~/Content/442styles/css").Include(    // Use with  =>  @Styles.Render("~/Content/442styles/css")       ///// BASE API ////
            "~/Content/442styles/jquery-ui-1.10.4.custom.css",
             "~/Content/442styles/bootstrap.css",
             ///// Custom CSS /////
            "~/Content/442styles/reset.css",
            "~/Content/442styles/fontface.css",
            "~/Content/442styles/layout.css" , 
             "~/Content/442styles/crudMVC4.css",  
            "~/Content/442styles/442Custom.css" ,
             "~/Content/442styles/jquery.bxslider.css",

            ///// Override  ///// 
             "~/Content/442styles/Override-bootstrap.css",
               "~/Content/442styles/Override-Jquery-UI.css" 
        ));

   
        //============================== 442.im  JS =========================//
        /////// Select  .js From  Scripts Folder 
        bundles.Add(new ScriptBundle("~/bundles/js").Include( 
                        "~/Scripts/bootstrap.js" , 
                        "~/Scripts/442Script/config-jqueryUiModal.js",
                        "~/Scripts/442Script/youtubeAPI.js",
                         "~/Scripts/bxslider.js", 
                        "~/Scripts/442Script/config-Bxslide.js" , 
                        "~/Scripts/442Script/_jsLibrary.js"  

        ));

       
  


        //======================= Toturail Bundle  Include &  Bundle IncludeDirectory =============================//
        /////////// Include  Select .css From  Content Folder
        // bundles.Add(new StyleBundle("~/Content/css").Include( // Use with  =>  @Styles.Render("~/Content/css")  
        // "~/Content/site.css"
        // ));  

        /////////// Include  Select .css From  Content Folder
        //bundles.Add(new StyleBundle("~/Content/442styles/css").Include(   // Use with  =>  @Styles.Render("~/Content/442styles/css")
        //"~/Content/442styles/layout.css",
        //"~/Content/442styles/reset.css",
        //"~/Content/442styles/bootstrap-plugin-offcanvas.css"
        //));  

        /////////// IncludeDirectory  -  Get All  .css From  442styles Folder  
        // bundles.Add(new StyleBundle("~/Content/442styles").IncludeDirectory("~/Content/442styles", "*.css")); // Use with => @Styles.Render("~/Content/442styles")

        //////////// IncludeDirectory  -  Get All  .css From  Content Folder (Not Include SubFolder)
        //  bundles.Add(new StyleBundle("~/Content/css").IncludeDirectory("~/Content/", "*.css", false));   // Use with  =>   @Styles.Render("~/Content/css") 

        //////////// IncludeDirectory  -  Get All  .css From  Content Folder (Include SubFolder)
        //  bundles.Add(new StyleBundle("~/Content/css").IncludeDirectory("~/Content/", "*.css", true));   // Use with  =>   @Styles.Render("~/Content/css") 





        }
    }
}