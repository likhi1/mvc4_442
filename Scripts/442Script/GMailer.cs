﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//================ 
using System.Net.Mail;
using System.Net;

namespace MVC442.Helper {
    public class GMailer { 

        /////////  Static Field (For Setup  SMTP  outside class )
        public static string MailUsername { get;  set; }
        public static string MailPassword { get; set; }
        public static string MailHost { get; set; }
        public static int MailPort { get; set; }
        public static bool MailSSL { get; set; }

        /////////  Public  Field ( For Add form requirement )
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; } 
        public string CC { get; set; }
        public string Bcc { get; set; }

        /////////  Constance   
        //========== Hostinguk.net  ============//
        public const string mailHost1 = "mail.4-4-2.fm";
        public const string mailUser1 = "admin@4-4-2.fm";
        public const string mailPass1 = "@uz6D4bZ8q7";   

        //========== AspNetHosting.co.uk  ============//
        public const string mailHost2 = "mail.4-4-2.im.ukwsp5.com";
        public const string mailUser2 = "admin@4-4-2.im.ukwsp5.com";
        public const string mailPass2 = "@9pTjr27Az1"; 
        
        public const string adminEmail1 = "lexdozy@hotmail.com";
        public const string adminEmail2 = "be2mail@gmail.com";
        public const string adminEmail3 = "likhi1@gmail.com"; 


        /// Set User & Pass for send mail by SMTP 
        static GMailer() {
            //======== Send Mail SMTP by Our Host  ( Hostinguk.net )
            MailHost = mailHost2;
            MailUsername = mailUser2 ;   //  OR noreply@ 
            MailPassword = mailPass2 ; 
            MailPort = 25;
            MailSSL = false;

            //======== Send Mail SMTP by Gmail
            //MailUsername = "xxxxx@gmail.com";
            //MailPassword = "xxxxxx";
            //MailHost = "smtp.gmail.com";  // smtp.gmail.com
            //MailPort = 587; // Gmail can use ports 25, 465 & 587; but must be 25 for medium trust environment.
            //MailSSL = true;
        }

        public void Send() {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = MailHost;
            smtp.Port = MailPort;
            smtp.EnableSsl = MailSSL;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(MailUsername, MailPassword);


            using ( var message = new MailMessage( ) ) {  // MailUsername, ToEmail
                message.From = new MailAddress(mailUser2, "442.im", System.Text.Encoding.UTF8);
                message.Subject = Subject;
                message.Body = Body;
                message.IsBodyHtml = IsHtml;

                //////////////
                // message.To.Add( ); // Not necessary to assign 
                message.CC.Add(CC); //  Set this send to user 
                message.Bcc.Add(adminEmail3);   //  Set this send to P'LEX  (Hide From User See)
               
                  
                smtp.Send(message); 

            }
        }
    }
}