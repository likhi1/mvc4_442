﻿
function DialogBasic(isOpen, isModal, dialogContentId, dialogButtonId, widthSize, heightSize, positionX, positionY, targetElement, addClass, isDrag) {
 
     isDrag != 'undefined' ? isDrag : false ;
   

    //========  Jquery UI-Dialog  LiveChat
    jQuery(dialogContentId).dialog({  
        show: "fade",
        hide: "fade",
        autoOpen: isOpen,
        dialogClass: addClass, // Fix position to bottom
        modal: isModal,
        width: widthSize,
        height: heightSize, 
        draggable: isDrag ,
        rsesizable: false,    // option   
        position: { my: positionX, at: positionY, of: targetElement },
        //position: { my: "right  top", at: "right   bottom", of: window },
        open: function () { // bind close when click overlay 
            jQuery(dialogContentId + " .buttonDialogClose").bind('click', function () {
                jQuery(dialogContentId).dialog('close');

                /// Insert  empty Link after click   .buttonDialogClose
                ClearUrlIframe(dialogContentId.replace('#', '') , "dialogRadio", "#radioArea  iframe");
                
            })
        }

    });

    ////// command event  dialog
    $(dialogButtonId).click(function (e) {
        //console.log(dialogButtonId+" Clicked");
        e.preventDefault();

        ////// Check If Dilog Of OTP will Assign  Link
        if (dialogButtonId == "#openDialogOTP") {
            OtpAssignLink();
        }

        $(dialogContentId).dialog('open');

    }); // end click

    /////// Config UI All 
    $(".ui-dialog-titlebar").hide();


} // end function DialogBasic


 

function DialogLiveChat(isOpen, isModal, dialogContentId, dialogButtonId, widthSize, heightSize, positionX, positionY, targetElement, addClass) {

    var btnLiveChat2 = $("#openDialogLiveChat2");

    //========  Jquery UI-Dialog  LiveChat
    jQuery(dialogContentId).dialog({
        show: { effect: "drop", direction: "down", duration: 1000 },
        hide: { effect: "drop", direction: "down", duration: 1000 },
        autoOpen: isOpen,
        dialogClass: addClass, // Fix position to bottom
        modal: isModal,
        width: widthSize,
        height: heightSize,
        //draggable: false ,
        rsesizable: false,    // option   
        position: { my: positionX, at: positionY, of: targetElement },
        //position: { my: "right  top", at: "right   bottom", of: window },
        open: function () { // bind close when click overlay 
            jQuery(dialogContentId + " .buttonDialogClose").bind('click', function () {
                jQuery(dialogContentId).dialog('close');
                btnLiveChat2.slideDown();
            })
        } 

    });

    ////// command event  dialog
    $(dialogButtonId).click(function (e) {
        e.preventDefault();

        //if(dialogButtonId=="#openDialogLiveChat1" || dialogButtonId=="#openDialogLiveChat2"  ){  //////  Check hide buttonLiveChat
        //	btnLiveChat2.slideDown();  
        //}  
        btnLiveChat2.slideUp();
        $(dialogContentId).dialog('open');

    }); // end click

    /////// Config UI All  
    $(".ui-dialog-titlebar").hide();

} // end function DialogLiveChat


///================   Extention Method For Dialog   ======================================================//
function OtpAssignLink(idButton) {

    //////  Set  src of DialogOTP
    var otpApp = 'http://www.appsmodule.4-4-2.im/CashCredit/Module/Clogin.aspx?telno=';
    var otpTelno = $("#telnoOTP").val();

    if ($("#telnoOTP").val() != "" && $("#telnoOTP").val() != null) {
        $("#dialogOTP iframe").attr('src', otpApp + otpTelno);
    } else {
        alert("กรุณากรอกข้อมูลให้ครบและถูกต้อง");
        return false;
    }
     
}


function ClearUrlIframe(strIdDilog, strIdDilogCommpare ,  strElm) {
    if (strIdDilog == strIdDilogCommpare) {
        var areaRadio = $( strElm );
        areaRadio.attr("src", "");
    }  

}





