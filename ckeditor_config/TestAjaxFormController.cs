﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//========== 
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using MVC442.Helper;
using MVC442.Models;
using MVC442.Models.ViewModel;

namespace MVC442.Controllers
{
    public class TestAjaxFormController : Controller
    {
        private mvc442Entities db = new mvc442Entities();

        public ActionResult OpenAccount(int id = 0)
        {
            ViewBag.CasinoCats = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh");
            var model = new ClientRegisterVM();  // Set Model by ViewModel
            return View(model);
        }

        [HttpPost]
        public ActionResult OpenAccount(ClientRegisterVM viewModel) {
            if (ModelState.IsValid) {

                var clientregister = new ClientRegister();
                //===  Set viewModel to  modelClass
                clientregister.IdCasinoCat = viewModel.IdCasinoCat;
                clientregister.Name = viewModel.Name;
                clientregister.Email = viewModel.Email;
                clientregister.Tel1 = viewModel.Tel1;
                clientregister.GetSms = viewModel.GetSms;
                clientregister.GetNews = viewModel.GetNews;

                //===== Set varaible 
                clientregister.InputDate = DateTime.Now;
                clientregister.Ip = Request.UserHostAddress;

                db.ClientRegisters.Add(clientregister);
                db.SaveChanges();

                //======= Test send same View
                ModelState.Clear();  //  If Return to Same View  have to  Clear ModelState 
                ViewData["resultPost"] = "You success to register";
                ViewBag.CasinoCats = new SelectList(db.CasinoCats, "IdCasinoCat", "CatNameTh");


                //return RedirectToAction("Index", "ClientRegister");
            }

            return View(viewModel);
        }





    }
}
